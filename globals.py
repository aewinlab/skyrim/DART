from pathlib import Path
import logging

from constants import SETTINGS_PATH, EXCLUSIONS_PATH, LANGUAGES_DIR, THEMES_DIR
from lib import Mod, AppComponents
from utils import darutils, file_io



user_data: list[Mod] = []
# MM_ROOT: Path = Path(__file__).parent.parent                # GET MO2_ROOT PATH WHEN INSTALLED CORRECTLY
# user_root: Path = Path(__file__).parent / "mo2"               # TEMPORARY MO2_ROOT PATH
# MO2_ROOT = Path(__file__).parent.parent / "[2] TESTS" / "mo2" # TEMPORARY MO2_ROOT PATH
app_components: "AppComponents | None" = None
user_root: Path = Path()
user_version: dict = {}
user_theme: dict = {}
user_settings: dict = {}
user_exclusions: dict = {}



def get_user_data() -> list[Mod]: return user_data

def set_user_data(new_value) -> None:
    global user_data
    user_data = new_value

def update_user_data() -> None:
    global user_data
    global user_root
    global user_exclusions
    user_data = darutils.get_conditions(user_root, exclude=user_exclusions)



def get_user_root() -> Path: return user_root

def set_user_root(new_value: Path) -> None:
    global user_root
    user_root = new_value



def get_user_version() -> dict: return user_version

def set_user_version(new_value: dict) -> None:
    global user_version
    user_version = new_value

def update_user_version() -> None:
    global user_version
    user_version = file_io.load_json(LANGUAGES_DIR / f"{user_settings.get('language')}.json")
    # new_version = file_io.load_json(LANGUAGES_DIR / f"{user_settings.get('language')}.json")
    # set_user_version(new_version)



def get_user_theme() -> dict: return user_theme

def set_user_theme(new_value: dict) -> None:
    global user_theme
    user_theme = new_value

def update_user_theme() -> None:
    global user_theme
    user_theme = file_io.load_json(THEMES_DIR / f"{user_settings.get('theme')}.json")



def get_user_settings() -> dict: return user_settings

def set_user_settings(new_value: dict) -> None:
    global user_settings
    user_settings = new_value

def save_user_settings() -> None:
    file_io.save_data(user_settings, SETTINGS_PATH)



def get_user_exclusions() -> dict: return user_exclusions

def set_user_exclusions(new_value: dict) -> None:
    global user_exclusions
    user_exclusions = new_value

def save_user_exclusions() -> None:
    file_io.save_data(user_exclusions, EXCLUSIONS_PATH)



# USELESS ???
def get_app_components() -> AppComponents: return app_components

def set_app_components(new_value: AppComponents) -> None:
    global app_components
    app_components = new_value

# def manage_app(component: str, action: str) -> None:
#     global app_components
#     if component == "main": pass
#     if component == "popup": pass
