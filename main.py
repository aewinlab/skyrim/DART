from pathlib import Path
import tkinter as tk
import logging

from constants import MO2_ROOT, SETTINGS_PATH, EXCLUSIONS_PATH, LANGUAGES_DIR, THEMES_DIR
from globals import get_user_data, set_user_data, get_user_root, set_user_root, update_user_version, update_user_theme, set_user_settings, set_user_exclusions, set_app_components
from lib import Condition, Mod, AppComponents
from utils import file_io, darutils
from gui import interface, excluder

# Name : DART - DAR tool



if __name__ == "__main__":
    # Get the user's saved settings
    user_settings = file_io.load_json(SETTINGS_PATH)
    set_user_settings(user_settings)
    # Get the user's version and theme based on saved settings
    update_user_version()
    update_user_theme()

    # Set default root for MO2 users
    set_user_root(MO2_ROOT)                 # Consider doing the same for other mod managers by incorporating the relevant default values in constants
    # Set user root based on either the default value or the custom value found in the user's settings
    ROOT = Path(user_settings.get('root')) if user_settings.get('root') else get_user_root()
    set_user_root(ROOT)

    # Get the user's exclusion list (mods & conditions)
    exclusions = file_io.load_json(EXCLUSIONS_PATH)
    set_user_exclusions(exclusions)

    # TO COMPLETE: Get exclusion auto source
    if user_settings.get('exclusions_auto') and user_settings.get('exclusions_auto_src'):
        # DO SOMETHING HERE: Get automatically exclusion list from user's manager source file
        # excluder.auto_update_exclusions_from_src() avec output={} et options={'no_window': True}
        pass

    # Get user's data from their mod manager's root directory
    output = darutils.get_conditions(ROOT, exclude=exclusions)
    set_user_data(output)                   #  Replace these 2 lines by update_user_data()?

    # Get all detected conditions & animations, and all duplicated priority values and corresponding mods from user's data
    # Export all user's data to interface
    # set_app_components(AppComponents(main=None, popup=None))
    interface.update_gui_data()




    # TESTING
    # interface.create_loader()
    #
    # darutils :
    # get_dar_mods() => rapide +++
    # get_conditions() => qq secondes (un peu plus long mais ok)
    # fetch_gui_data() => qq secondes (un peu plus long mais ok)

    # interface :
    # log_detected_duplicates() => long +++
    # A COMMENTER POUR ACCELERER +++ :
    # for priority in duplicates:
    #         for mod_name in duplicates[priority]:
    #             log(f"Detected a duplicated priority: {priority} | {mod_name}")





    # Create interface with user's data
    interface.create_interface()


    # Add a way to save data somewhere? (json file?) And settings?
    # # file_io.save_data()

# python3.11.0 64bits
