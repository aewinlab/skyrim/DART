import tkinter as tk
import logging
from datetime import datetime

# from constants import _HB, _HT
from globals import get_user_root, get_user_theme
from lib import TkWidget



# logging.basicConfig(level=logging.DEBUG)



_WT = ""



def update_logger_theme():                                     # TO COMMENT (previously get_color(key: str) which returned return colors.get(key))
    global _WT

    colors = get_user_theme().get('colors')                                         # UI['theme'] = get_user_theme()

    # Set UI theme values                                                           # f.bg, f.fg (f. = frame)
    _WT = colors['widget_text']                                                     # text for widget items



def clean_widgets(widgets: list[TkWidget], options={}):     # MISSING DOCSTRING AND COMMENTS
    for widget in widgets:
        if type(widget.content) == tk.Text: widget.content.delete(1.0, tk.END)
        elif type(widget.content) in [tk.Listbox, tk.Entry]: widget.content.delete(0, tk.END)
        elif type(widget.content) == tk.Canvas: widget.content.delete(options.get('tag'))
        else: print(f"FUNCTION clean_widgets(): OTHER WIDGET TYPE DETECTED = {type(widget.content)}")       # DEBUG



def find_element_index(element_path, element_name: str, element_type: str, widget: tk.Listbox):             #MISSING DOCSTRING + ANNOTATIONS

    ROOT = get_user_root()

    for index in range(widget.size()):
        current_element = widget.get(index)

        # Search for elements within the widget with a name matching the desired element name
        if current_element == element_name:

            # If the element is a Mod, return the index that corresponds to the searched element name
            if element_type == "mod": return index

            # If the element is a Condition, inspect previous elements and return the index of the element that shares the same name and originates from the same mod as the target
            if element_type == "condition":
                    # Store the index of the current element as a possible result to be returned later if it meets the desired criteria
                    element_found_index = index

                    # Test previous elements until finding one that originates from the same mod as the searched element name (using its dir_path)
                    for i in range(index):
                        previous_element = widget.get(index - (i + 1))
                        if previous_element == element_path.relative_to(ROOT).parts[0]: return element_found_index



def force_selection(widget: TkWidget, selection: "int | str", options={}):      # MISSING DOCSTRING & DOCUMENTATION   # handle_selection() ?   # Added "" because of '|' not being accepted for exe creation (py-to-exe is py3.9 and introduction of '|' for types in python3.10+)
    # Listbox: listbox.selection_clear(0, "end") puis listbox.selection_set(0)
    # OptionMenu: option_variable.set(value)

    # Si le widget est un menu déroulant (categories) et que la sélection est une str (stringvar)
    if type(widget.content) == tk.OptionMenu and type(selection) == str: widget.options['stringvar'].set(selection)

    # Si le widget est une listbox (parameters & results) et que la sélection est un int (index)
    elif type(widget.content) == tk.Listbox and type(selection) == int:

        # USEFUL??? TESTING PURPOSE - Change background/foreground color for selected items, based on retrieved values from stored items previously selected by the user
        if options.get('highlight_previous'):
            # Set UI theme values                                                           # f.bg, f.fg (f. = frame)
            colors = get_user_theme().get('colors')
            _HB = colors['highlighted_background']                           # bg for highlighted items
            _HT = colors['highlighted_text']                                 # text for highlighted items
            for i in range(widget.content.size()): widget.content.itemconfigure(i, bg="", fg="")
            widget.content.itemconfigure(selection, bg=_HB, fg=_HT)

        # Efface la sélection actuelle dans la listbox
        widget.content.selection_clear(0, "end")
        widget.content.activate(selection)
        widget.content.selection_set(selection)

        # Si une fonction callback est présente dans les options, l'appelle
        if options.get('callback'):
            callback = options['callback']

            # Si un fake_event est présent dans les options, créé un tel event et l'utilise comme argument à passer à la fonction callback
            if options.get('fake_event'):
                new_event = tk.Event()                                                                  # new_event = options['fake_event']
                new_event.widget = widget.content
                callback(new_event)                                                                     # lambda: callback(new_event)
            else:
                callback()



def log_into_widget(text: str, data: dict, widget: TkWidget) -> None:                     # MISSING DOCSTRING
    # data : gui_data
    # widget : widgets_group_1['logs']

    # No log entry will be recorded if the global gui_data 'no_log' value is set to True
    if data.get('no_log'): return

    # Add the new log to the beginning of the logs list, then format the list as a string for display in the logger widget
    time = datetime.now()
    data['logs'].insert(0, f"{time.strftime('%H:%M:%S')} : {text}")
    all_logs = "\n".join(data['logs'])

    # Clear any previous content in the logger widget and then display all logs, including the newest one, in the widget
    clean_widgets([widget], options={'tag': "logs"})
    widget.content.create_text(10, 10, text=all_logs, fill=_WT, tags="logs", anchor="nw")

    # Update the scroll region of the logger widget to fit its content
    bbox = widget.content.bbox("logs")
    widget.content.config(scrollregion=bbox)

    # Conventionally log the new log text to be saved in a log file and for debugging purposes
    logging.info(text)
    print(text)                                 # DEBUG
