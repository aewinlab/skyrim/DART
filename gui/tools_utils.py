import tkinter as tk

from globals import get_user_root, get_user_theme, get_user_settings
from lib import TkWidget, Mod, Condition
from utils.file_io import save_text, get_lines, clean_lines
from utils.maths import uniform_randomizer
from gui import cache
from gui.helpers import clean_widgets


_WT = ""

def update_tools_theme():                                     # TO COMMENT (previously get_color(key: str) which returned return colors.get(key))
    global _WT

    colors = get_user_theme().get('colors')                                         # UI['theme'] = get_user_theme()

    # Set UI theme values                                                           # f.bg, f.fg (f. = frame)
    _WT = colors['widget_text']                                                     # text for widget items



def clear_items(input: dict) -> None:
    input_widget1 = input['widget']['input']          # input TkWidget (ex input)
    input_widget2 = input['widget']['preview']          # input TkWidget (ex input)
    input_data = input['data']              # input data (ex current_tool)

    clean_widgets([input_widget1, input_widget2], options={'tag': "preview_changes"})
    if input_data.get('content'): input_data['content'] = []

    # ADD LOG HERE



def add_item(input: dict, output: dict) -> None:
    input_widget = input['widget']          # input TkWidget (ex input)
    input_data = input['data']              # input data (ex user_selection)
    output_widget = output['widget']        # output TkWidget (ex widgets_group_2['input'])
    output_data = output['data']            # output data (ex current_tool)

    # Get user selection from input widget
    selected_element = input_widget.content.curselection()
    if not selected_element or not input_data.get('result'): return
    # selected_index = selected_element[0]
    selected_element = input_widget.content.get(selected_element)                           # Get selected element value from curselection tuple (NB : (tuple)[0])

    current_elements = [int(element.split(" | ")[0]) for element in output_widget.content.get(0, tk.END) if str(element.split(" | ")[0]).isdigit()]

    if input_data['result'].get('type') == "condition":

        ROOT = get_user_root()
        selected_element_path = input_data['result']['data'].path
        selected_element_mod = selected_element_path.relative_to(ROOT).parts[0]

        # if not f"{selected_element} | {selected_element_mod}" in widgets_group_2['input'].content.get(0, tk.END):
        if not int(selected_element) in current_elements:
            output_widget.content.insert(tk.END, f"{selected_element} | {selected_element_mod}")
            output_widget.content.itemconfig(tk.END, fg=_WT)
            output_data['content'].append({'priority': int(selected_element), 'path': selected_element_path})

            # ADD LOG HERE
    
    # DONE! = PREVOIR ADDITION DE TOUS LES ELEMENTS D'UN MOD SI MOD CHOISI : user_selection.get('result').get('type') == "mod" VERSUS "condition"
    elif input_data['result'].get('type') == "mod" and input_data['result'].get('data'):

        mod_name = input_data['result']['data'].name

        for condition in input_data['result']['data'].conditions:

            # if not f"{condition.priority} | {mod_name}" in widgets_group_2['input'].content.get(0, tk.END):
            if not condition.priority in current_elements:
                output_widget.content.insert(tk.END, f"{condition.priority} | {mod_name}")
                output_widget.content.itemconfig(tk.END, fg=_WT)
                output_data['content'].append({'priority': condition.priority, 'path': condition.path})
                
                # ADD LOG HERE
    
    # else: ADD LOG HERE(gestion des doublons / éléments déjà présents dans la listbox)



def remove_item(input: dict) -> None:
    input_widget = input['widget']          # input TkWidget (ex input)
    input_data = input['data']              # input data (ex current_tool)

    # Get user selection from input widget
    selected_element = input_widget.content.curselection()
    if not selected_element: return             # ADD LOG HERE (nothing selected)?
    selected_index = selected_element[0]
    selected_element = input_widget.content.get(selected_element)
    if selected_element.split(" | ")[0].isdigit():
        selected_element_priority = int(selected_element.split(" | ")[0])
    else: return
    input_widget.content.delete(selected_index)

    # RETIRER L'ELEMENT CORRESPONDANT DE current_tool['content']
    for element in input_data['content']:
        if element['priority'] == selected_element_priority: input_data['content'].remove(element)

    # ADD LOG HERE



def move_item_up(input: TkWidget) -> None:
    # Get user selection from input widget
    selected_element = input.content.curselection()
    if not selected_element: return             # ADD LOG HERE (nothing selected)?
    selected_index = selected_element[0]
    if selected_index == 0: return              # ADD LOG HERE (first item selected, can't move it up)?
    selected_element = input.content.get(selected_element)
    # previous_element = input.content.get(selected_index - 1)
    # previous_index = input.content.index(previous_element)
    previous_index = selected_index - 1
    # previous_element = input.content.get(previous_index)
    input.content.delete(selected_index)
    input.content.insert(previous_index, selected_element)

    input.content.itemconfig(previous_index, fg=_WT)

    input.content.selection_clear(0, tk.END)
    input.content.activate(previous_index)
    input.content.select_set(previous_index, last=None)

    # ADD LOG HERE



def move_item_down(input: TkWidget) -> None:
    # Get user selection from input widget
    selected_element = input.content.curselection()
    if not selected_element: return             # ADD LOG HERE (nothing selected)?
    selected_index = selected_element[0]
    if selected_index == input.content.size() - 1: return              # ADD LOG HERE (last item selected, can't move it down)?
    selected_element = input.content.get(selected_element)
    next_index = selected_index + 1
    input.content.delete(selected_index)
    input.content.insert(next_index, selected_element)

    input.content.itemconfig(next_index, fg=_WT)

    input.content.selection_clear(0, tk.END)
    input.content.activate(next_index)
    input.content.select_set(next_index, last=None)

    # ADD LOG HERE



def preview_changes(input: dict, output: TkWidget) -> None:
    # input (ex current_tool)
    # output TkWidget (ex widgets_group_2['preview'])

    # selected_items = [item for item in input.content.get(0, tk.END) if item.isdigit()]      #GENERIC METHOD: selected_items = list(input.content.get(0, tk.END))
    selected_items = [item.get('priority') for item in input.get('content') if type(item.get('priority')) == int]

    options = {'is_using_base': True} if get_user_settings().get('randomizer_uses_base_animation') == True else {}
    output_data = uniform_randomizer(selected_items, options=options)

    text = "\n".join([f"{i['priority']}: Random({i['proba']}){' or nothing' if i['proba'] == 1 else ''}" for i in output_data if i['priority'] != "base"])
    # text = "\n".join([f"{priority}: Random({output[priority]})" for priority in output if priority != "base"])
    clean_widgets([output], options={'tag': "preview_changes"})
    output.content.create_text(3, 3, text=text, fill=_WT, tags="preview_changes", anchor="nw")

    # ADD LOG HERE

    print(input['content'])          # DEBUG
    # REFACTORER AVEC current_tool['content'] ; print(user_selection.get('result').get('data'))     # Condition() avec .path ==> .path / "_conditions.txt" ==> Add "Random(value) AND" en premier élément/première ligne
    # AJOUTER LE NOM DES MODS DANS LE WIDGET INPUT ? AVEC "priority | mod" ?



def apply_changes(input: dict) -> None:
    selected_items = [item.get('priority') for item in input.get('content') if type(item.get('priority')) == int]

    options = {'is_using_base': True} if get_user_settings().get('randomizer_uses_base_animation') == True else {}
    output = uniform_randomizer(selected_items, options=options)
    
    # Add corresponding calculated probability to each item
    for i in output:
        for item in input['content']:
            if i.get('priority') == item.get('priority'):
                item['proba'] = i.get('proba')
                break
    
    # Add "Random(value) AND" (or without "AND" if no other line) as the first line of each item condition file (item.path / "_conditions.txt"), or log an error
    for item in input['content']:
        if item.get('proba'):               # compléter le fichier _conditions.txt correspondant
            conditions_file_path = item.get('path') / "_conditions.txt"
            conditions_by_line = get_lines(conditions_file_path)
            cleaned_conditions_by_line = clean_lines(conditions_by_line)                        # FAIRE PAREIL DANS on_button_clicked() POUR ALLEGER CODE

            # Check if "Random(" est déjà présent dans le fichier de conditions, et le supprime le cas échéant
            for line in cleaned_conditions_by_line:
                if line.lower().find("random") != -1: cleaned_conditions_by_line.remove(line)
            if len(cleaned_conditions_by_line) == 0: cleaned_conditions_by_line.append(f"Random({item['proba']})")
            else: cleaned_conditions_by_line.insert(0, f"Random({item['proba']}) AND")

            ########## IDEM QUE on_button_clicked() => A SIMPLIFIER EN CREANT UNE FONCTION ET EN L'APPLIQUANT ICI ET LA-BAS ##########
            # Set edited conditions as the new value for current conditions (if edit_conditions button clicked) -- A RESTRUCTURER SELON ACTION BOUTON ?
            if get_user_settings().get('cache') == True: cache.update_edited_conditions(item.get('path'), cleaned_conditions_by_line)   # TO COMMENT: use cache only if cache function is enabled
            # Save user text input into _conditions.txt file of the selected priority
            text = "\n".join(cleaned_conditions_by_line)
            save_text(text, conditions_file_path)
            # Add a new log entry to the logger widget & log file
            ROOT = get_user_root()

            # ADD LOG HERE
            # log(f"Saved new conditions for priority #{item['priority']} from mod {item['path'].relative_to(ROOT).parts[0]}")
            print(f"Saved new conditions for priority #{item['priority']} from mod {item['path'].relative_to(ROOT).parts[0]}")      # DEBUG (while log is not reimplemented)

            # PENSER A REFRESH LA ZONE D'ENTRY DANS LE CAS OU L'ELEMENT SELECTIONNE DANS L'OUTPUT DE LA FENETRE PRINCIPALE EST L'UN DE CEUX EDITES PAR apply_changes()

        else: pass                          # ADD LOG : bad format / unable to add random()
    
    # ADD LOG HERE ? OU A CHAQUE ITERATION CI-DESSUS ?

    # clean_widget ? clean current_tool['content'] = [] ? Plutot ajouter un bouton "X" en haut à droite (et un <> à gauche pour switcher de tool)













    ##### IDEM on_button_clicked() #####
    # new_conditions = input.content.get(1.0, tk.END)

    # # Clean conditions from useless characters (' ', '\n')
    # new_conditions_by_line = []                                                                         ### COMING FROM DARUTILS
    # for line in new_conditions.splitlines():
    #     stripped_line = line.strip(" ")
    #     if stripped_line in ("\n", ""): continue
    #     new_conditions_by_line.append(stripped_line)
    
    # # Set edited conditions as the new value for current conditions (if edit_conditions button clicked) -- A RESTRUCTURER SELON ACTION BOUTON ?
    # if get_user_settings().get('cache') == True: cache.update_edited_conditions(dir_path, new_conditions_by_line)   # TO COMMENT: use cache only if cache function is enabled

    # # Save user text input into _conditions.txt file of the selected priority
    # text = "\n".join(new_conditions_by_line)
    # save_text(text, conditions_file_path)

    # # Add a new log entry to the logger widget & log file
    # log(f"Saved new conditions for priority #{user_focus['text']} from mod {dir_path.relative_to(ROOT).parts[0]}")



    ##### IDEM on_result_selected() #####
    # # Retrieve the content of the _conditions.txt file and display it within the appropriate output widget (text input field)
    # if user_focus['type'] == "condition":
    #     selected_condition = user_focus['data']
    #     conditions_by_lines = get_lines(selected_condition.path / "_conditions.txt")
    #     text = "\n".join(conditions_by_lines)
    #     outputs[1].content.insert(tk.END, text)
    #     outputs[1].content.config(fg=_E2T)