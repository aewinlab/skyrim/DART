from pathlib import Path
import logging

from globals import get_user_data, set_user_data, get_user_root
from utils.file_io import strip_words



######################### TO USE FOR OPTIMIZATION #########################
# A UTILISER COMME UNE OPTION A DEFINIR ("use caching") ?
###########################################################################

def update_renamed_mod(dir_path, new_dirname):                 # dans on_button_clicked
    # 1. trouver le mod dans globals/user_data, avec son nom actuel (user_focus['data'].name) ou son path (dir_path)
    ROOT = get_user_root()
    user_data = get_user_data()

    for mod in user_data:
        # if mod.name == user_focus['data'].name:    # OU   # if mod.path == dir_path:
        # if mod.path.relative_to(ROOT).parts[0] == dir_path.relative_to(ROOT).parts[0]: test = mod.path.relative_to(dir_path)
        if mod.name == dir_path.relative_to(ROOT).parts[0]:

            # 2. actualiser le nom et le path du mod = mod.path = nouveau_nom      (qui est un Path = (dir_path.parent / new_dirname))
            mod.name = new_dirname
            mod.path = (dir_path.parent / new_dirname / mod.path.relative_to(dir_path))

            # 3. actualiser le path de chaque condition contenue dans le dossier du mod = condition.path = nouveau_nom (qui est un Path)
            for condition in mod.conditions:
                condition.path = (mod.path / str(condition.priority))
            
            # # 4. Rename the current folder with the user entry input
            # dir_path.rename(dir_path.parent / new_dirname)

            # 5. update user data
            set_user_data(user_data)
            return          # interrompt la boucle                      # return mod


def update_renamed_priority(dir_path, new_dirname):     # dans on_button_clicked
    # 1. trouver le mod dans globals/user_data, avec son path actuel (dir_path (ou conditions_file_path qui = dir_path / "_conditions.txt"))
    ROOT = get_user_root()
    user_data = get_user_data()

    for mod in user_data:
        if mod.name == dir_path.relative_to(ROOT).parts[0]:         # trouvé !

            # 2. trouver la condition mod dans globals/user_data, avec son nom actuel
            # condition.path = path    # condition.priority = int     #  dir_path = str   #  dir_path.parts[-1] = str   # new_dirname = str
            for condition in mod.conditions:
                if condition.path == dir_path:              # if condition.priority == int(dir_path.parts[-1]):

                    # 3. actualiser le nom et le path de la priorité = mod.path = nouveau_nom      (qui est un Path = (dir_path.parent / new_dirname))
                    condition.priority = int(new_dirname)       # prendre en compte si is_digit (via try/except) (ancien : pas de int())
                    condition.path = (dir_path.parent / new_dirname)

                    # # 4. Rename the current folder with the user entry input
                    # dir_path.rename(dir_path.parent / new_dirname)

                    # 5. update user data
                    set_user_data(user_data)
                    return          # interrompt la boucle                      # return condition


def update_edited_conditions(dir_path, new_conditions_by_line):
    # 1. trouver le mod dans globals/user_data, avec son path actuel (dir_path (ou conditions_file_path qui = dir_path / "_conditions.txt"))
    ROOT = get_user_root()
    user_data = get_user_data()

    for mod in user_data:
        if mod.name == dir_path.relative_to(ROOT).parts[0]:         # trouvé !

            # 2. trouver la condition mod dans globals/user_data, avec son nom actuel
            for condition in mod.conditions:
                if condition.priority == dir_path:

                    # 3. actualiser le contenu des conditions de la condition = condition.conditions = nouvelles_conditions      (qui est une liste)
                    conditions_by_line = []                                 # environ idem que dans get_conditions() de darutils
                    for line in new_conditions_by_line:
                        stripped_line = strip_words(line, ("AND", "OR", " "))
                        if stripped_line not in conditions_by_line: conditions_by_line.append(stripped_line)
                    condition.conditions = conditions_by_line
                    # FAIRE UN 5. AUSSI ? set_user_data(user_data) ; probablement pas car reload le fichier lui-meme et pas d'utilité au cache ici
                    return          # interrompt la boucle                      # return condition
    # penser à fetch_gui_data() après pour réactualiser le contenu de l'interface
###########################################################################

# penser à sort() les mods/priorities pour réactualiser le contenu de l'interface correctement (perdu en cas d'utilisation du cache)
# PENSER A GERER LES ERREURS (error handling) AVEC UN LOG QUI INCITE A RELOAD LES DATA (FAIRE UN BOUTON OU DANS MENU)