# Pour les settings et customisations de l'interface
from pathlib import Path
import tkinter as tk
from tkinter import filedialog

from globals import get_user_root, set_user_root, get_user_settings, set_user_settings, save_user_settings



def change_language(language: tk.StringVar):            # MISSING DOCSTRING & DOCUMENTATION
    user_settings = get_user_settings()
    user_settings['language'] = language.get()
    set_user_settings(user_settings)
    save_user_settings()

    # update_user_version()
    # update_language()

    # ADD A LOG HERE



def change_theme(theme: tk.StringVar):                  # A FAIRE / REGROUPABLE AVEC change_language ?
    user_settings = get_user_settings()
    user_settings['theme'] = theme.get()
    set_user_settings(user_settings)
    save_user_settings()

    # global UI
    # UI['window'].configure(background="black")
    # widgets_group_1['categories'].frame.config(background="black")

    # ADD A LOG HERE



def change_root():                                      # A FAIRE / REGROUPABLE AVEC change_language/theme ?
    ROOT = get_user_root()

    dir_path = filedialog.askdirectory(initialdir=str(ROOT))
    set_user_root(Path(dir_path))

    user_settings = get_user_settings()
    user_settings['root'] = str(get_user_root())
    set_user_settings(user_settings)
    save_user_settings()

    # ADD A LOG HERE



def toggle_cache():            # MISSING DOCSTRING & DOCUMENTATION
    user_settings = get_user_settings()
    
    if user_settings.get('cache') == True: user_settings['cache'] = False
    else: user_settings['cache'] = True

    set_user_settings(user_settings)
    save_user_settings()

    # print(get_user_settings().get('cache'))     # DEBUG

    # update_user_version()
    # update_language()

    # ADD A LOG HERE



def toggle_sidebar():            # MISSING DOCSTRING & DOCUMENTATION
    user_settings = get_user_settings()
    
    if user_settings.get('sidebar') == True: user_settings['sidebar'] = False
    else: user_settings['sidebar'] = True

    set_user_settings(user_settings)
    save_user_settings()

    # print(get_user_settings().get('cache'))     # DEBUG

    # update_user_version()
    # update_language()

    # ADD A LOG HERE



def toggle_duplicates_highlight():            # MISSING DOCSTRING & DOCUMENTATION
    user_settings = get_user_settings()
    
    if user_settings.get('higlight_duplicates') == True: user_settings['higlight_duplicates'] = False
    else: user_settings['higlight_duplicates'] = True

    set_user_settings(user_settings)
    save_user_settings()

    # print(get_user_settings().get('cache'))     # DEBUG

    # update_user_version()
    # update_language()

    # ADD A LOG HERE



#############################################################################################
# OTHER FILE POUR TOOLS SETTINGS ?

def toggle_randomizer_uses_base_animation():
    user_settings = get_user_settings()
    
    if user_settings.get('randomizer_uses_base_animation') == True: user_settings['randomizer_uses_base_animation'] = False
    else: user_settings['randomizer_uses_base_animation'] = True

    set_user_settings(user_settings)
    save_user_settings()
    
    # update_user_version()
    # update_language()

    # ADD A LOG HERE
