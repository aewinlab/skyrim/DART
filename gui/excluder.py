from pathlib import Path
from typing import Callable
import tkinter as tk
from tkinter import filedialog
import logging
from datetime import datetime

# from constants import MO2_ROOT, LANGUAGES_DIR, THEMES_DIR
from globals import get_user_data, update_user_data, get_user_root, set_user_root, get_user_version, set_user_version, update_user_version, get_user_theme, update_user_theme, get_user_settings, set_user_settings, save_user_settings, get_user_exclusions, set_user_exclusions, save_user_exclusions
from lib import TkWidget, Mod, Condition
from utils import darutils
from utils.file_io import save_data, save_text, get_lines, clean_lines
# from utils.maths import uniform_randomizer
# from gui import cache
# from gui.config import change_language, change_theme, change_root, toggle_cache, toggle_sidebar, toggle_randomizer_uses_base_animation
from gui.helpers import clean_widgets, find_element_index, force_selection, log_into_widget, update_logger_theme
from gui.tools_utils import add_item, remove_item, move_item_up, move_item_down, preview_changes, apply_changes, clear_items, update_tools_theme



_BG = _WB = _WT = ""
WIDGETS: dict[TkWidget, list[TkWidget]] = { 'active_mods': {}, 'excluded_mods': {} }



def update_excluder_theme() -> None:                                     # TO ADD INTO INTERFACE.UPDATE_THEME()
    """Update the theme of the excluder and allow this component to use their needed UI theme parameters."""

    global _BG, _WB, _WT

    colors = get_user_theme().get('colors')

    # Set the color values for various UI elements corresponding to the selected theme (called with f.bg, f.fg... (f. = frame))
    _BG = colors['main_background']                                          # Main background color
    _WB = colors['widget_background']                                        # Background color for widgets
    _WT = colors['widget_text']                                              # Text color for items in widgets


def update_excluder_widgets() -> dict:
    """Update the excluder widgets with the lists of mods that have been marked as either active or excluded by the user."""

    active_mods = [mod.name for mod in get_user_data()]
    excluded_mods = get_user_exclusions().get('mods')

    return {'active': active_mods, 'excluded': excluded_mods}


def save_exclusions(data: list) -> None:
    """Save a list of excluded mods into exclusions.json file.

    Args:
        data (list): The list of all detected DAR mods that will be excluded from DART interface."""
    
    # Set new user exclusions into its globals' value
    user_exclusions = get_user_exclusions()
    user_exclusions['mods'] = data
    set_user_exclusions(user_exclusions)

    # Save the new settings into exclusions.json
    save_user_exclusions()


def apply_exclusions(input: TkWidget, callback: Callable, options: dict  = {}) -> None:
    """Apply the user exclusions by saving them to 'exclusions.json' and close the excluder window.

    Args:
        input (TkWidget): The Widget containing the user's exclusion list."""
    
    # Get excluded mods from corresponding widget and save them
    exclusion_list = [item for item in input.content.get(0, tk.END)]
    save_exclusions(exclusion_list)

    # Close the excluder and open appropriate window (main window if no arg is given)
    if options.get('arg'): callback(options.get('arg'))
    else: callback()


def transfer_item(input: TkWidget, output: TkWidget) -> None:
    # Get user selection from input widget
    selected_element = input.content.curselection()
    # if not selected_element or not item:
    if not selected_element:
        # print("rien de sélectionné")
        return
    selected_index = selected_element[0]
    selected_element = input.content.get(selected_element)                           # Get selected element value from curselection tuple (NB : (tuple)[0])

    # if last selected, next selection will be previous one
    next_selected_index = 0
    if selected_index == input.content.size() - 1:
        next_selected_index = selected_index - 1
        # print("last selected")      # DEBUG
    else: next_selected_index = selected_index
    
    # if not selected_element in output.content:      # USELESS ???
    output.content.insert(tk.END, f"{selected_element}")
    output.content.itemconfig(tk.END, fg=_WT)
    # IL FAUT RETIRER L'ELEMENT DE L'INPUT WIDGET
    input.content.delete(selected_index)
    input.content.selection_clear(0, tk.END)
    # Set new selected item as active
    input.content.select_set(next_selected_index, last=None)
    input.content.activate(next_selected_index)

    # SORT ALPHABETICALLY? CREATE A NEW FUNCTION IN HELPERS OR ELSEWHERE?
    # ADD LOG HERE


def auto_update_exclusions_from_src(output: dict, options={}) -> None:
    # Get user's manager file that acts as the source to automatically get exclusions
    exclusions_auto_src = Path(get_user_settings().get('exclusions_auto_src'))

    # Check if the source file is valid
    if not exclusions_auto_src.is_file(): return

    # Get user's data from their mod manager's root directory
    user_root = get_user_root()
    user_exclusions = get_user_exclusions()
    no_exclusion = {key: [] for key in user_exclusions}
    user_mods = [mod.name for mod in darutils.get_conditions(user_root, exclude=no_exclusion)]

    # Clean active and excluded mods widgets and data if needed
    excluded_mods = []
    active_mods = []
    if not options.get('no_window'):
        widget_active = output['active']
        widget_excluded = output['excluded']
        clean_widgets([widget_active, widget_excluded])

    src_content_by_line = get_lines(exclusions_auto_src)
    for line in src_content_by_line:
        if line.startswith("-"):
            if line.lstrip("-") in user_mods:
                excluded_mods.append(line.lstrip("-"))

                # Check if function is called from excluder window
                if options.get('no_window'): continue

                widget_excluded.content.insert(tk.END, f"{line.lstrip('-')}")
                widget_excluded.content.itemconfig(tk.END, fg=_WT)

        elif line.startswith("+"):
            if line.lstrip("+") in user_mods:
                active_mods.append(line.lstrip("+"))

                # Check if function is called from excluder window
                if options.get('no_window'): continue

                widget_active.content.insert(tk.END, f"{line.lstrip('+')}")
                widget_active.content.itemconfig(tk.END, fg=_WT)

    # Set first item of active mods as active if needed
    if not options.get('no_window'):
        widget_active.content.selection_clear(0, tk.END)
        widget_active.content.select_set(0, last=None)
        widget_active.content.activate(0)
    
    save_exclusions(excluded_mods)


def change_exclusions_auto_src_path() -> None:                          # change current mo2 profile
    # Get Mod Manager profiles folder path  (for MO2 users)
    PROFILES = get_user_root().parent / "profiles"                      # For MO2 users modlist_path = PROFILES / Default (or profile_name) / "modlist.txt"
    if PROFILES.is_dir():
        dir_path = filedialog.askdirectory(initialdir=str(PROFILES))
        exclusions_auto_src = Path(dir_path) / "modlist.txt"            # For MO2 users (exclusions_auto_src = manager_modlist_path)
    
    # Alternates for non-MO2 users
    else:
        PROFILES = get_user_root().parent                               # To ensure other mod manager users could resolve function (uncertainty about 'profiles' folder)
        dir_path = filedialog.askdirectory(initialdir=str(PROFILES))
        exclusions_auto_src = Path(dir_path)                            # !!! TO COMPLETE FOR OTHER SITUATIONS !!!

    # Set new user exclusions into settings globals' value
    user_settings = get_user_settings()
    user_settings['exclusions_auto_src'] = str(exclusions_auto_src)
    set_user_settings(user_settings)

    # Save the new settings into settings.json
    save_user_settings()

    # ADD A LOG HERE
