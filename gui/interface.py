from pathlib import Path
import tkinter as tk
from tkinter import filedialog
import logging
from datetime import datetime

from constants import MO2_ROOT, LANGUAGES_DIR, THEMES_DIR
from globals import get_user_data, update_user_data, get_user_root, set_user_root, get_user_version, set_user_version, update_user_version, get_user_theme, update_user_theme, get_user_settings, set_user_settings, save_user_settings, get_user_exclusions
from lib import TkWidget, Mod, Condition
from utils import darutils
from utils.file_io import save_text, get_lines, clean_lines
from utils.maths import uniform_randomizer
from gui import cache
from gui.config import change_language, change_theme, change_root, toggle_cache, toggle_sidebar, toggle_duplicates_highlight, toggle_randomizer_uses_base_animation
from gui.helpers import clean_widgets, find_element_index, force_selection, log_into_widget, update_logger_theme
from gui.tools_utils import add_item, remove_item, move_item_up, move_item_down, preview_changes, apply_changes, clear_items, update_tools_theme
from gui.excluder import update_excluder_theme, update_excluder_widgets, transfer_item, apply_exclusions, save_exclusions, change_exclusions_auto_src_path, auto_update_exclusions_from_src



UI: dict = {'window': {}, 'text': {}, 'theme': {}}
_BG = _TX = _WB = _WT = _WO = _AB = _AT = _HB = _HT = _SB = _ST = _DB = _DT = _I1B = _I1T = _I2B = _I2T = _B1B = _B1T = _B2B = _B2T = _E1T = _E2T = ""
gui_data: dict[dict[list]] = {'categories': {}, 'duplicates': {}, 'logs': [], 'no_log': False}
widgets_group_1: dict[TkWidget, list[TkWidget]] = {'categories': {}, 'parameters': {}, 'results': {}, 'inputs': {}, 'buttons': {}, 'logs': {}}
widgets_group_2: dict[TkWidget, list[TkWidget]] = {'input': {}, 'preview': {}, 'buttons': {}}
widgets_excluder: dict[TkWidget, list[TkWidget]] = { 'active_mods': {}, 'excluded_mods': {}, 'buttons': {} }
user_selection: dict = {'category': {}, 'parameter': {}, 'result': {}}
current_tool: dict = {'content': []}



def log(text: str) -> None:                     # MISSING DOCSTRING
    global gui_data, widgets_group_1
    if not widgets_group_1.get('logs'): return
    log_into_widget(text, gui_data, widgets_group_1['logs'])


def update_ui(component: str = "") -> None:                                # MISSING DOCSTRING & DOCUMENTATION
    # ALTERNATIVE en mettant a jour chaque widget, necessite de rework le set_filter en transformant fonction en update_widget_language(widget: TkWidget)
    # widget.name = UI['text']['results']['header']
    # widget.frame.config(text=widget.name)
    # Necessiterait aussi de gerer le tracking existant pour les categories avec trace(), trace_info(), trace_vdelete(), puis de set_filter a nouveau pour refresh
    # for language in LANGUAGES_DIR.glob("*"): LANGUAGES_MENU.add_command(label=language.stem)
    # SETTINGS_MENU.options['stringvar'].trace("w", lambda name, index, mode, var=SETTINGS_MENU: on_language_changed())

    # Detruit la fenetre
    UI['window'].destroy()                                  # UI['window'].quit() only close window loop processus but the window stays open & active
    # print(f"UPDATING INTERFACE... NEW COMPONENT: {component}")      # DEBUG

    # update entierement les donnees de l'app
    update_user_data()
    update_user_version()
    update_user_theme()
    update_gui_data()

    # reset les valeurs devant être reset
    global current_tool
    current_tool = {'content': []}
    # ??? user_selection ???

    # évite de dupliquer les duplicates dans le logger et efface les logs
    # gui_data['logs'] = []
    # # # ALTERNATIVE : Utiliser gui_data['no_log'] = True via create_interface ou log_duplicates
    # # # global gui_data   # gui_data['no_log'] = True     # current_logs = gui_data.get('logs')   # gui_data['no_log'] = False

    # Rouvre le component de l'app correspondant
    if component == "excluder": create_excluder()
    else: create_interface()

    # ADD A LOG HERE ? BEFORE ?


def update_gui_data() -> None:
    """Update the GUI-exclusive data (used by the interface) with the latest user data, excluding items specified in their exclusion list. Called for example after saving user's changes."""

    global gui_data                                                                             # Refactoring needed (and main.py & darutils.py) for better optimization?

    # Store previous logs to guarantee they are retained and can be reincorporated after updating gui_data
    current_logs = gui_data.get('logs')

    # Get the user's exclusion list (mods & conditions)
    exclusions = get_user_exclusions()

    # Update GUI data from saved user data and re-assign previous logs to the updated GUI data
    user_data = get_user_data()
    gui_data = darutils.fetch_gui_data(user_data, exclude=exclusions)
    gui_data['logs'] = current_logs
    gui_data['no_log'] = False


def update_language():                          # MISSING DOCSTRING     # TO FINISH (+ group with theme/root...) / RENAME ? (load_lang() ?) / USEFUL ?
    global UI, gui_data

    UI['text'] = get_user_version()

    if not UI['text'].get('categories') or not gui_data.get('categories'): return

    gui_categories = {}
    # gui_categories = { UI['text']['categories']['options'][key]: category for category in gui_data.get('categories') for key in UI['text']['categories']['options'] if category.lower() == key.lower() }
    for category in gui_data.get('categories'):
        for key in UI['text']['categories']['options']:
            if category.lower() == key.lower():
                gui_categories[key] = { 'header': category,
                                        'new_header': UI['text']['categories']['options'][key],
                                        'data': gui_data['categories'][category] }
            # else: print("NON: ", category, key)

    # UI['text'] = get_user_version()

    gui_data['categories'] = {}
    for key in gui_categories: gui_data['categories'][gui_categories[key]['new_header']] = gui_categories[key]['data']

    # ADD A LOG HERE

def update_theme() -> None:                                     # REGROUPABLE AVEC update_language ? RENAME EN load_theme() (ainsi que les fonctions dedans) ?
    """Updates the theme of the user interface based on the selected theme in the UI settings."""

    global UI, _BG, _TX, _WB, _WT, _WO, _AB, _AT, _HB, _HT, _SB, _ST, _DB, _DT, _I1B, _I1T, _I2B, _I2T, _B1B, _B1T, _B2B, _B2T, _E1T, _E2T
    UI['theme'] = get_user_theme()

    # Set the color values for various UI elements corresponding to the selected theme (called with f.bg, f.fg... (f. = frame))
    _BG = UI['theme']['colors']['main_background']                                          # Main background color
    _TX = UI['theme']['colors']['main_text']                                                # Main text color
    _WB = UI['theme']['colors']['widget_background']                                        # Background color for widgets
    _WT = UI['theme']['colors']['widget_text']                                              # Text color for items in widgets
    _WO = UI['theme']['colors']['widget_outline']                                           # Outline color for widgets
    _AB = UI['theme']['colors']['active_background']                                        # Background color for active widgets
    _AT = UI['theme']['colors']['active_text']                                              # Text color for items in active widgets
    _HB = UI['theme']['colors']['highlighted_background']                                   # Background color for highlighted items
    _HT = UI['theme']['colors']['highlighted_text']                                         # Text color for highlighted items
    _SB = UI['theme']['colors']['section_background']                                       # Background color for title items separating widgets in sections
    _ST = UI['theme']['colors']['section_text']                                             # Text color for title items separating widgets in sections
    _DB = UI['theme']['colors']['duplicates_background']                                    # Background color for duplicated items
    _DT = UI['theme']['colors']['duplicates_text']                                          # Text color for duplicated items
    _I1B = UI['theme']['colors']['input_background']['rename_folder']                       # Background color for user's input 'Rename'
    _I1T = UI['theme']['colors']['input_text']['rename_folder']                             # Text color for title of user's input 'Rename'
    _I2B = UI['theme']['colors']['input_background']['edit_conditions']                     # Background color for user's input 'Edit'
    _I2T = UI['theme']['colors']['input_text']['edit_conditions']                           # Text color for title of user's input 'Edit'
    _B1B = UI['theme']['colors']['button_background']['rename_folder']                      # Background color for user's button 'Rename'
    _B1T = UI['theme']['colors']['button_text']['rename_folder']                            # Text color for user's button 'Rename'
    _B2B = UI['theme']['colors']['button_background']['edit_conditions']                    # Background color for user's button 'Edit'
    _B2T = UI['theme']['colors']['button_text']['edit_conditions']                          # Text color for user's button 'Edit'
    _E1T = UI['theme']['colors']['entry_text']['rename_folder']                             # Text color for content of user's input 'Rename'
    _E2T = UI['theme']['colors']['entry_text']['edit_conditions']                           # Text color for content of user's input 'Edit'

    # Update the theme of each app's component
    update_logger_theme()
    update_tools_theme()
    update_excluder_theme()


def log_detected_duplicates():                        # MISSING DOCSTRING & DOCUMENTATION             # CREATE A BUTTON TO REDETECT NEW DUPLICATES, CALLING THIS FUNCTION?
    """Log any detected duplicate entries of priority numbers in the user data."""
    # update_gui_data()                                                                         # TO CALL IF NOT PREVIOUSLY
    duplicates = gui_data.get('duplicates')

    for priority in duplicates:
            for mod_name in duplicates[priority]:
                log(f"Detected a duplicated priority: {priority} | {mod_name}")


def configure_ui(setting: str, new_value=None) -> None:
    """Call specific functions to configure the user interface according to the specified setting. Update the UI afterwards.

    Args:
        setting (str): The name of the setting to be configured. Valid values: "root", "lang", "theme", "cache", "duplicates", "sidebar".
        new_value (optional): The new value for the setting, if applicable."""

    if setting == "root": change_root()
    elif setting == "lang": change_language(new_value)
    elif setting == "theme": change_theme(new_value)
    elif setting == "duplicates": toggle_duplicates_highlight()
    elif setting == "cache": toggle_cache()
    elif setting == "sidebar": toggle_sidebar()

    update_ui()


def configure_tool(setting: str, option: str = None, new_value=None) -> None:                     # regrouper avec configure_ui ?
    """Call specific functions to configure the user tools according to the specified setting. Update the UI afterwards.

    Args:
        setting (str): The name of the tool to be configured. Valid values: "randomizer".
        option (str, optional): A sub-setting or option within the tool. Defaults to None.
        new_value (optional): The new value for the setting, if applicable."""

    if setting == "randomizer":
        if option == "is_using_base": toggle_randomizer_uses_base_animation()
    
    update_ui()


def set_filter() -> None:                                   # Trace option selected in input menu to update output widget
    """Sets up a filter for the data displayed on the user interface.
    When user changes selected category in the input, on_category_changed() is called to update the data displayed in the output."""

    global widgets_group_1
    input = widgets_group_1['categories']

    input.options['stringvar'].trace("w", lambda name, index, mode, var=input: on_category_changed())

    set_output()


def set_output() -> None:                                   # Bind click event to the input list and its output widget
    """Sets up the output display according to the user's selection.
    When user makes a selection in the input, on_select() is called to update the data displayed in the output."""

    global widgets_group_1
    input = widgets_group_1['parameters']

    input.content.bind("<<ListboxSelect>>", lambda event: on_element_selected(event))

    set_results_outputs()


def set_results_outputs() -> None:                          # Bind click event to the input list and its output widget
    """Sets up the outputs display according to the user's selection.
    When user makes a selection in the input, on_result_selected() is called to update the data displayed in the output."""

    global widgets_group_1
    input = widgets_group_1['results']

    input.content.bind("<<ListboxSelect>>", lambda event: on_result_selected(event))


def on_category_changed() -> None:                          # Update output TkWidget content with adapted data when user changes active category in input TkWidget
    """Updates the output content with adapted data when the user changes the active category in the input widget.
    When user changes selected category in the input, output list widget content is updated according to the new input menu option selected, and set_output() is called to update the next widget display according to the user's selection in the output."""

    global gui_data, widgets_group_1, user_selection
    input = widgets_group_1['categories']
    output = widgets_group_1['parameters']
    data = gui_data.get('categories')

    # Get user selection from input widget
    selected_category = input.options['stringvar'].get()

    # Store information about the category currently selected by the user
    user_selection['category'] = {'index': None, 'text': selected_category}

    # Update the output list widget content according to the new input menu option selected
    output.new_list(data[selected_category], options={'w.bg': _WB, 'w.fg': _WT})
    output.name = selected_category
    output.frame.config(text=selected_category)

    # Rebind click event to the updated output list as its new input and next widget as its output
    next_widgets = [widgets_group_1['results'], widgets_group_1['inputs'][0], widgets_group_1['inputs'][1]]
    if next_widgets: clean_widgets(next_widgets)
    set_output()

    # Add a new log entry to the logger widget & log file
    log(f"Selected a category: {selected_category}")


def on_element_selected(event: tk.Event) -> None:           # Update output TkWidget content with adapted data when user selects an element in input TkWidget
    """Updates the output content with adapted data when the user selects an element in the input widget.
    When user makes a selection in the input, updates the data (mod names and DAR priority directories) displayed in the output widget according to the user's selection."""

    global widgets_group_1, user_selection
    input = widgets_group_1['parameters']
    output = widgets_group_1['results']
    categories = UI['text']['categories']['options']

    # Get user selection from input widget
    selected_element = event.widget.curselection()
    if not selected_element: return
    selected_index = selected_element[0]
    selected_element = event.widget.get(selected_element)                           # Get selected element value from curselection tuple (NB : (tuple)[0])

    # Store information about the parameter currently selected by the user and change its background/foreground color to keep it visible when selecting an item in the child widget
    for i in range(input.content.size()): input.content.itemconfigure(i, bg=_WB, fg=_WT)
    input.content.itemconfigure(selected_index, bg=_HB, fg=_HT)
    user_selection['parameter'] = {'index': selected_index, 'text': selected_element, 'output': []}

    # Clean all the child widgets from their content
    next_widgets = [widgets_group_1['inputs'][0], widgets_group_1['inputs'][1]]
    if next_widgets: clean_widgets(next_widgets)
    if output: clean_widgets([output])

    # Get the relevant elements (priorities and the mod they belong) corresponding to the parameter selected by the user from user data
    for mod in get_user_data():
        for condition in mod.conditions:
            if input.name.lower() == categories['conditions'].lower(): condition_elements = condition.conditions                # If selected parameter is from "Conditions" category
            elif input.name.lower() == categories['animations'].lower(): condition_elements = condition.animations              # If selected parameter is from "Animations" category
            elif input.name.lower() == categories['mods'].lower(): condition_elements = [mod.name]                              # If selected parameter is from "Mods" category
            elif input.name.lower() == categories['priorities'].lower(): condition_elements = [str(condition.priority)]         # If selected parameter is from "Priorities" category
            else: condition_elements = []

            # Search for the selected element inside the data, then append mod name and DAR priority directory to content list if selected element is found
            if selected_element.lower() in [element.lower() for element in condition_elements]:
                if not f"{mod.name}" in output.content.get(0, tk.END):
                    output.content.insert(tk.END, f"{mod.name}")
                    output.content.itemconfig(tk.END, bg=_SB, fg=_ST, selectbackground="blue", selectforeground="white")
                    user_selection['parameter']['output'].append({"text": mod.name, "type": "mod", "data": mod})
                output.content.insert(tk.END, f"{str(condition.priority)}")
                if get_user_settings().get('higlight_duplicates') == True and condition.priority in gui_data.get('duplicates'): output.content.itemconfig(tk.END, bg=_DB, fg=_DT)
                else: output.content.itemconfig(tk.END, fg=_WT)
                user_selection['parameter']['output'].append({"text": condition.priority, "type": "condition", "data": condition})
    
    # Add a new log entry to the logger widget & log file
    log(f"Selected a{'n' if input.name.lower() == 'animations' else ''} {'priority' if input.name.lower() == 'priorities' else input.name.lower()[:-1]}: {selected_element}")


def on_result_selected(event: tk.Event) ->  None:           # Update outputs TkWidget content with adapted data when user selects a result in input TkWidget
    """Updates the ouputs content with adapted data when the user selects an element in the input widget.
    When user makes a selection in the input, updates the data (mod or priority folder name and conditions file content) displayed in the outputs widget according to the user's selection."""

    global widgets_group_1, user_selection
    outputs = widgets_group_1['inputs']

    # Get user selection from input widget
    selected_result = event.widget.curselection()
    if not selected_result: return
    selected_element = event.widget.get(selected_result)                            # !!! selected_element stores DAR priority as a str, not an int !!!
    selected_index = selected_result[0]

    # Store information about the result currently selected by the user
    user_focus = user_selection['parameter']['output'][selected_index]
    user_selection['result'] = {'index': selected_index, 'text': selected_element, 'type': user_focus['type'], 'data': user_focus['data']}

    # Clean all the child widgets from their content
    if outputs: clean_widgets(outputs)

    # Display the DAR priority within the appropriate output widget (entry input field)
    outputs[0].content.config(fg=_E1T)
    outputs[0].content.insert(tk.END, selected_element)

    # Retrieve the content of the _conditions.txt file and display it within the appropriate output widget (text input field)
    if user_focus['type'] == "condition":
        selected_condition = user_focus['data']
        conditions_by_lines = get_lines(selected_condition.path / "_conditions.txt")
        text = "\n".join(conditions_by_lines)
        outputs[1].content.insert(tk.END, text)
        outputs[1].content.config(fg=_E2T)
    
    # Add a new log entry to the logger widget & log file
    log(f"Selected a {'priority' if user_focus['type'] == 'condition' else 'mod' if user_focus['type'] == 'mod' else ''} folder: {selected_element}")


def on_button_clicked(input: TkWidget, action: str = ""):        # MISSING DOCSTRING AND COMMENTS -- save_changes()
    global user_selection
    user_focus = user_selection['result']

    # Check if the user has selected an item in the results_display widget
    if not user_focus: return

    # Get the current folder/file path depending on the selected element type (mod or condition)
    ROOT = get_user_root()
    if user_focus['type'] == "mod": dir_path = ROOT / user_focus['data'].name       # dir_path = mod folder
    elif user_focus['type'] == "condition":
        dir_path = user_focus['data'].path                                              # dir_path = priority folder (condition.path)
        conditions_file_path = dir_path / "_conditions.txt"                             # conditions_file_path = path to _conditions.txt for the selected priority
    

    # If rename_button is clicked (alternate version might be used if problem when renaming root directory without renaming its subdirectories)
    if action == "rename" and type(input.content) == tk.Entry:
        new_dirname = input.content.get()

        if get_user_settings().get('cache') == True:                                      # TO COMMENT: use cache only if cache function is enabled
            if user_focus['type'] == "mod": cache.update_renamed_mod(dir_path, new_dirname)
            elif user_focus['type'] == "condition": cache.update_renamed_priority(dir_path, new_dirname)

        # Rename the current folder with the user entry input
        dir_path.rename(dir_path.parent / new_dirname)

        # Add a way to manage error for the case if new mod/priority dirname is already existing

        # Add a new log entry to the logger widget & log file
        log(f"Renamed the {'priority' if user_focus['type'] == 'condition' else 'mod' if user_focus['type'] == 'mod' else ''} folder {user_focus['text']} to {new_dirname}")
    
    # If edit_conditions_button is clicked
    elif action == "edit_content" and user_focus['type'] == "condition" and type(input.content) == tk.Text:
        new_conditions = input.content.get(1.0, tk.END)

        # Clean conditions from useless characters (' ', '\n')
        new_conditions_by_line = []                                                                         ### COMING FROM DARUTILS
        for line in new_conditions.splitlines():
            stripped_line = line.strip(" ")
            if stripped_line in ("\n", ""): continue
            new_conditions_by_line.append(stripped_line)
        
        # Set edited conditions as the new value for current conditions (if edit_conditions button clicked) -- A RESTRUCTURER SELON ACTION BOUTON ?
        if get_user_settings().get('cache') == True: cache.update_edited_conditions(dir_path, new_conditions_by_line)   # TO COMMENT: use cache only if cache function is enabled

        # Save user text input into _conditions.txt file of the selected priority
        text = "\n".join(new_conditions_by_line)
        save_text(text, conditions_file_path)

        # Add a new log entry to the logger widget & log file
        log(f"Saved new conditions for priority #{user_focus['text']} from mod {dir_path.relative_to(ROOT).parts[0]}")
    
    # If another button is clicked or another input widget type is detected
    else: log("ERROR: Unrecognized Action or Input widget type detected upon Button Click")


    # Update user data & GUI data after saving changes made by the user
    if get_user_settings().get('cache') != True: update_user_data()                         # TO COMMENT: reload everything only if cache function is disabled
    update_gui_data()


    # TESTING PURPOSE
    selected_category = widgets_group_1['categories'].options['stringvar'].get()                # rename selected_category_text for more clarity ?

    # TESTING PURPOSE - Retrieve stored items previously selected by the user
    selected_parameter_index = user_selection['parameter'].get('index')             #

    clean_widgets([widgets_group_1['parameters'], widgets_group_1['results']])

    gui_data['no_log'] = True

    force_selection(widgets_group_1['categories'], selected_category)
    force_selection(widgets_group_1['parameters'], selected_parameter_index, options={'callback': on_element_selected, 'fake_event': True, 'highlight_previous': True})

    # TO COMMENT: depending on the button clicked, set value to force selection of edited result (new_dirname is not set if save _conditions.txt file button clicked)
    if action == "rename": selected_result_index = find_element_index(dir_path, new_dirname, user_focus['type'], widgets_group_1['results'].content)
    elif action == "edit_content": selected_result_index = user_selection['result'].get('index')
    force_selection(widgets_group_1['results'], selected_result_index, options={'callback': on_result_selected, 'fake_event': True})

    gui_data['no_log'] = False



def create_interface() -> None:                             # Create interface for the whole application
    """Creates the main interface for the application using tkinter library.
    Generates a window, its grid, and content based on TkWidgets (tkinter-derived complex objects), sets user's interaction with these elements, and pack all of them to display the interface."""

    global UI, widgets_group_1

    update_language()
    update_theme()
    categories = gui_data.get('categories')

    # Create main window (with its menu bar) and its rows and grid to hold the interface sections
    UI['window'] = tk.Tk()
    UI['window'].title("DART - DAR manager for TES V : Skyrim")
    # UI['window'].protocol("WM_DELETE_WINDOW", exit)                                               # Disabled because calling exit() without ending windows mainloop caused issues
    MENU_BAR = tk.Menu(UI['window'])
    UI['window'].config(menu=MENU_BAR)
    tk_window = tk.PanedWindow(UI['window'], orient=tk.HORIZONTAL, bg=_BG)
    tk_window.pack(fill=tk.BOTH, expand=True)
    tk_rows = tk.PanedWindow(tk_window, orient=tk.VERTICAL, bg=_BG)
    tk_rows.pack(fill=tk.BOTH, expand=True)
    tk_grid = tk.PanedWindow(tk_rows, orient=tk.HORIZONTAL, bg=_BG)
    tk_grid.pack(fill=tk.BOTH, expand=True)
    tk_grid2 = tk.PanedWindow(tk_rows, orient=tk.HORIZONTAL, bg=_BG)
    tk_grid2.pack(fill=tk.BOTH, expand=True)

    # Create a dropdown menu widget for user to select a filter category ("Animations" or "Conditions")
    category_menu = TkWidget(name=UI['text']['categories']['header'])
    category_menu.create_frame(tk_grid, "menu", categories, options={'f.bg': _BG, 'f.fg': _TX, 'w.bg': _WB, 'w.fg': _WT, 'w.bg:a': _AB, 'w.fg:a': _AT, 'w.hb': _WO})

    # Create a clickable list widget for user to select a search parameter (an animation file or a condition eg DAR functions)
    parameters_list = TkWidget(name=category_menu.options['stringvar'].get())
    parameters_list.create_frame(tk_grid, "list", categories[parameters_list.name], options={'f.bg': _BG, 'f.fg': _TX, 'w.fg':_WT})
    parameters_list.content.config(width=75, height=20, bg=_WB)

    # Create a section to display results from search as a widget (sorted by mod name and priority directory)
    results_display = TkWidget(name=UI['text']['results']['header'])
    results_display.create_frame(tk_grid, "list", [], options={'f.bg': _BG, 'f.fg': _TX})
    results_display.content.config(width=100, height=20, bg=_WB)

    # Create two input widgets for the user to either rename DAR priority folders and/or edit conditions files
    user_input = TkWidget(name=UI['text']['inputs']['user_input']['header'])
    user_input.create_frame(tk_grid2, "entry", "", options={'f.bg': _BG})
    user_input.content.config(width=102, bg=_I1B)
    user_input.frame.configure(font=("TkDefaultFont", 10, "bold"), fg=_I1T)
    user_conditions = TkWidget(name=UI['text']['inputs']['user_conditions']['header'])
    user_conditions.create_frame(tk_grid2, "text", "", options={'f.bg': _BG})
    user_conditions.content.config(width=75, height=15, bg=_I2B)
    user_conditions.frame.configure(font=("TkDefaultFont", 10, "bold"), fg=_I2T)

    # Create two buttons in a dedicated frame to validate user input for renaming DAR folders and/or editing conditions files
    user_buttons = TkWidget(name=UI['text']['buttons']['header'])
    user_buttons.create_frame(user_input.frame, "", "", options={'f.bg': _BG, 'f.fg': _TX}) # param2 = "" or "label" ?
    user_buttons.frame.config(labelanchor='n')                                              # configure(padx=50, pady=50) ? config(width=102) ?
    rename_button = TkWidget(name="Rename DAR folder (<Priority> number or Mod name)")
    rename_button.new_button(UI['text']['buttons']['rename_button'], options={'parent': user_buttons.frame, 'command': {'function': on_button_clicked, 'input': user_input, 'type': 'rename'}})
    rename_button.content.config(width=32, font=("TkDefaultFont", 10, "bold"), bg=_B1B, fg=_B1T, activebackground="lightblue", activeforeground="navy")
    edit_conditions_button = TkWidget(name="Edit conditions file (_conditions.txt)")
    edit_conditions_button.new_button(UI['text']['buttons']['edit_conditions_button'], options={'parent': user_buttons.frame, 'command': {'function': on_button_clicked, 'input': user_conditions, 'type': 'edit_content'}})
    edit_conditions_button.content.config(width=32, font=("TkDefaultFont", 10, "bold"), bg=_B2B, fg=_B2T, activebackground="lightpink", activeforeground="darkred")

    # Create a logger section to display all logs, events and errors
    logs_display = TkWidget(name=UI['text']['logs']['header'])
    logs_display.create_frame(tk_rows, "canvas", "", options={'f.bg': _BG, 'f.fg': _TX, 'w.bg': _WB})

    
    # ###########################################  MENU  BAR  ###########################################
    # Create a menu bar at the top of the main window for user to change settings and customize the interface (+ bg=_WB to apply to all tk.Menus?)

    #region - Menus > File
    FILE_MENU = tk.Menu(MENU_BAR, tearoff=0)
    MENU_BAR.add_cascade(label=UI['text']['menus']['file']['header'], menu=FILE_MENU)

    #region - Menus > File > Refresh data
    FILE_MENU.add_command(label=UI['text']['menus']['file']['options']['reload'], command=update_ui)
    #endregion

    FILE_MENU.add_separator()

    #region - Menus > File > Manage exclusion lists
    FILE_MENU.add_command(label=UI['text']['menus']['file']['options']['exclusions'], command=lambda: update_ui(component="excluder"))
    #endregion

    FILE_MENU.add_separator()

    FILE_MENU.add_command(label=UI['text']['menus']['file']['options']['import'], state="disabled")                             # FUTURE FEATURE (TEMPLATE)
    FILE_MENU.add_command(label=UI['text']['menus']['file']['options']['export'], state="disabled")                             # FUTURE FEATURE (TEMPLATE)

    FILE_MENU.add_separator()

    #region - Menus > File > Exit
    # Rather using destroy() because: quit() doesn't close the app but only windowloop; exit() invokes 'from within' errors if windowloop still not destroyed
    FILE_MENU.add_command(label=UI['text']['menus']['file']['options']['exit'], command=UI['window'].destroy)
    #endregion

    #endregion

    #region - Menus > Settings
    SETTINGS_MENU = tk.Menu(MENU_BAR, tearoff=0)
    MENU_BAR.add_cascade(label=UI['text']['menus']['settings']['header'], menu=SETTINGS_MENU)

    #region - Menus > Settings > Change root path
    SETTINGS_MENU.add_command(label=UI['text']['menus']['settings']['options']['root'], command=lambda: configure_ui("root"))
    #endregion

    SETTINGS_MENU.add_separator()

    #region - Menus > Settings > Change language
    LANGUAGES_MENU = tk.Menu(SETTINGS_MENU, tearoff=0)
    SETTINGS_MENU.add_cascade(label=UI['text']['menus']['settings']['options']['lang'], menu=LANGUAGES_MENU)

    languages = [language.stem for language in LANGUAGES_DIR.glob("*")]
    selected_language = tk.StringVar()
    selected_language.set(get_user_settings()['language'])
    for language in languages: LANGUAGES_MENU.add_radiobutton(label=language, command=lambda: configure_ui("lang", selected_language), variable=selected_language)
    #endregion

    #region - Menus > Settings > Change theme
    THEMES_MENU = tk.Menu(SETTINGS_MENU, tearoff=0)
    SETTINGS_MENU.add_cascade(label=UI['text']['menus']['settings']['options']['theme'], menu=THEMES_MENU)

    themes = [theme.stem for theme in THEMES_DIR.glob("*")]
    selected_theme = tk.StringVar()
    selected_theme.set(get_user_settings()['theme'])
    for theme in themes: THEMES_MENU.add_radiobutton(label=theme, command=lambda: configure_ui("theme", selected_theme), variable=selected_theme)
    #endregion

    SETTINGS_MENU.add_separator()

    #region - Menus > Settings > Highlight duplicates
    is_higlighting_duplicates = tk.BooleanVar()
    is_higlighting_duplicates.set(get_user_settings()['higlight_duplicates'])
    SETTINGS_MENU.add_radiobutton(
        label="{}{}".format(UI['text']['menus']['settings']['options']['duplicates'], '  [\u2713]' if get_user_settings().get('higlight_duplicates') else '  [   ]'),
        command=lambda: configure_ui("duplicates"), variable=is_higlighting_duplicates)
    #endregion

    SETTINGS_MENU.add_separator()

    #region - Menus > Settings > Use cache function
    is_using_cache = tk.BooleanVar()
    is_using_cache.set(get_user_settings()['cache'])
    SETTINGS_MENU.add_radiobutton(
        label="{}{}".format(UI['text']['menus']['settings']['options']['cache'], '  [\u2713]' if get_user_settings().get('cache') else '  [   ]'),
        command=lambda: configure_ui("cache"), variable=is_using_cache)
    #endregion

    #endregion

    #region - Menus > Tools
    TOOLS_MENU = tk.Menu(MENU_BAR, tearoff=0)
    MENU_BAR.add_cascade(label=UI['text']['menus']['tools']['header'], menu=TOOLS_MENU)

    TOOLS_MENU.add_command(label=UI['text']['menus']['tools']['options']['preview'], state="disabled")                          # FUTURE FEATURE (TEMPLATE)
    TOOLS_MENU.add_command(label=UI['text']['menus']['tools']['options']['prob_calc'], state="disabled")                        # FUTURE FEATURE (TEMPLATE)

    TOOLS_MENU.add_separator()

    #region - Menus > Tools > Randomize base animation
    is_randomizer_using_base = tk.BooleanVar()
    is_randomizer_using_base.set(get_user_settings()['randomizer_uses_base_animation'])
    TOOLS_MENU.add_radiobutton(
        label="{}{}".format(UI['text']['menus']['tools']['options']['randomizer_opt1'], '  [\u2713]' if get_user_settings().get('randomizer_uses_base_animation') else '  [   ]'),
        command=lambda: configure_tool("randomizer", "is_using_base"), variable=is_randomizer_using_base)
    #endregion

    #endregion

    #region - Menus > Views
    VIEWS_MENU = tk.Menu(MENU_BAR, tearoff=0)
    MENU_BAR.add_cascade(label=UI['text']['menus']['views']['header'], menu=VIEWS_MENU)

    #region - Menus > Views > Show sidebar
    is_displaying_sidebar = tk.BooleanVar()
    is_displaying_sidebar.set(get_user_settings()['sidebar'])
    VIEWS_MENU.add_radiobutton(
        label="{}{}".format(UI['text']['menus']['views']['options']['sidebar'], '  [\u2713]' if get_user_settings().get('sidebar') else '  [   ]'),
        command=lambda: configure_ui("sidebar"), variable=is_displaying_sidebar)
    #endregion

    VIEWS_MENU.add_separator()

    VIEWS_MENU.add_command(label="Option 1", state="disabled")                                                                  # FUTURE FEATURE (TEMPLATE)
    VIEWS_MENU.add_command(label="Option 2", state="disabled")                                                                  # FUTURE FEATURE (TEMPLATE)

    #endregion

    # ###################################################################################################


    widgets_group_1 = { 'categories': category_menu,                                            # ADD widgets_group_2 pour la sidebar ?
                        'parameters': parameters_list,
                        'results': results_display,
                        'inputs': [user_input, user_conditions],
                        'buttons': [rename_button, edit_conditions_button],
                        'logs': logs_display
                        }
    

    # Setting up the filter and the output display according to user's parameter selection and corresponding data
    set_filter()                                            # TO RENAME?

    # Display detected mods with DAR priority duplicates in the logger widget
    log_detected_duplicates()

    # Add graphical elements to the grid and pack all widgets into window, to place and display them all together in the interface
    category_menu.frame.pack()              # drop-down menu to choose a category
    parameters_list.frame.pack()            # list of parameters to filter the results
    results_display.frame.pack()            # zone to display the results corresponding to the user's choices
    user_buttons.frame.pack(pady=15)
    rename_button.content.pack(side=tk.LEFT, padx=35, pady=10)
    edit_conditions_button.content.pack(side=tk.RIGHT, padx=35, pady=10)
    logs_display.frame.pack()
    
    tk_grid.add(category_menu.frame)
    tk_grid.add(parameters_list.frame)
    tk_grid.add(results_display.frame)
    tk_grid2.add(user_input.frame)
    tk_grid2.add(user_conditions.frame)
    tk_rows.add(tk_grid)
    tk_rows.add(tk_grid2)
    tk_rows.add(logs_display.frame)
    tk_window.add(tk_rows)

    if get_user_settings().get('sidebar'): create_sidebar(tk_window)                # CONDITIONNE L'AFFICHAGE DE LA SIDEBAR A UNE OU DES OPTION(S)

    # Start the main loop
    UI['window'].mainloop()


def create_sidebar(window: tk.PanedWindow) -> None:         # Create a sidebar to add to the right side of the application's interface
    """Create a sidebar to add on the right side of the application using tkinter library.
    Generates a preview window, a batch of tools with each one's content being based on TkWidgets (tkinter-derived complex objects), handle user's interactions with these elements, and pack all of them to display them into the sidebar."""

    global widgets_group_2, current_tool

    def button_handler(input: str, action: str) -> None:
        """Handle button actions in the UI by calling specific functions that perform specific tasks, according to a given input tool and action.

        Args:
            input (str): The name of the tool calling its own actions. Valid values: "randomizer".
            action (str): The name of the specific action. Values based on the given input."""
        
        if input == "randomizer":
            if action == "add": add_item(
                input={'widget': widgets_group_1['results'], "data": user_selection},
                output={'widget': widgets_group_2['input'], 'data': current_tool}
                )
            elif action == "remove": remove_item(input={'widget': widgets_group_2['input'], 'data': current_tool})
            elif action == "move_up": move_item_up(input=widgets_group_2['input'])
            elif action == "move_down": move_item_down(input=widgets_group_2['input'])
            elif action == "preview": preview_changes(input=current_tool, output=widgets_group_2['preview'])
            elif action == "apply": apply_changes(input=current_tool)
            elif action == "clear": clear_items(input={'widget': widgets_group_2, 'data': current_tool})

        else: return            # TO COMPLETE WITH OTHER TOOLS      # print(current_tool)     # DEBUG
    
    # Create the sidebar into parent window
    tk_sidebar = tk.PanedWindow(window, orient=tk.VERTICAL, bg=_BG)
    tk_sidebar.pack(fill=tk.BOTH, expand=True)

    # ########################################  PREVIEW  WINDOW  ########################################
    # Create a section for preview window (allowing to preview either folder content (hkx / txt files) and preview selected hkx animation)

    #region Preview window (WIP)
    preview = TkWidget(name=UI['text']['tools']['preview'])
    preview.create_frame(tk_sidebar, "canvas", "", options={'no_scrollbar': True, 'f.bg': _BG, 'f.fg': _TX, 'w.bg': _WB})
    preview.content.config(width=450, height=450)
    preview.frame.pack()
    preview.content.pack(fill=tk.BOTH, expand=True)
    #endregion

    # ###################################################################################################


    # ########################################### BATCH TOOLS ###########################################
    # Create a section for batch tools (toolbox with different tools)
    batch_tools = TkWidget(name=UI['text']['tools']['header'])        # Tools or Batch Tools?
    batch_tools.create_frame(tk_sidebar, "label", UI['text']['tools']['randomizer'], options={'is_header': True, 'f.bg': _BG, 'f.fg': _TX})

    #region Randomizer tool (widgets)

    # Create an input section for user to select items to apply active tool's effect to
    batch_input = TkWidget(name="Batch input")
    batch_input.create_frame(batch_tools.frame, "list", [], options={'no_label': True, 'f.bg': _BG})
    batch_input.content.config(width=70, height=7, bg=_WB)


    # Create a preview section to display the expected result before applying active tool's effect
    batch_result_preview = TkWidget(name="Preview output")
    batch_result_preview.create_frame(batch_tools.frame, "canvas", "", options={'no_label': True, 'f.bg': _BG, 'w.bg': _WB})
    batch_result_preview.content.config(width=420, height=150)
    
    
    # Create buttons frame to pack the main set of active tool's buttons
    batch_buttons = TkWidget(name="Toolbox Buttons")
    batch_buttons.create_frame(batch_tools.frame, "", "", options={'no_label': True, 'f.bg': _BG})

    # Create a button to clean the active tool's input list
    batch_button_clear = TkWidget(name="Clear items")
    batch_button_clear.new_button(UI['text']['tools']['buttons']['clear'], options={
        'parent': batch_tools.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'randomizer', 'type': 'clear'}})
    
    # Create two buttons to add and remove a selected item (eg a priority folder) to the tool's input list
    batch_button_add = TkWidget(name="Add item")
    batch_button_add.new_button(UI['text']['tools']['buttons']['add'], options={
        'parent': batch_buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'randomizer', 'type': 'add'}})
    
    batch_button_remove = TkWidget(name="Remove item")
    batch_button_remove.new_button(UI['text']['tools']['buttons']['remove'], options={
        'parent': batch_buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'randomizer', 'type': 'remove'}})
    
    # Create two buttons to move up and move down a selected item (eg a priority folder) in the tool's input list
    batch_button_move_up = TkWidget(name="Move up item")
    batch_button_move_up.new_button(UI['text']['tools']['buttons']['move_up'], options={
        'parent': batch_buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'randomizer', 'type': 'move_up'}})
    
    batch_button_move_down = TkWidget(name="Move down item")
    batch_button_move_down.new_button(UI['text']['tools']['buttons']['move_down'], options={
        'parent': batch_buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'randomizer', 'type': 'move_down'}})
    
    # Create a button to preview the result of the active tool before applying its effect
    batch_button_preview = TkWidget(name="Preview changes")
    batch_button_preview.new_button(UI['text']['tools']['buttons']['preview'], options={
        'parent': batch_buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'randomizer', 'type': 'preview'}})
    
    # Create a button to apply the effect of the active tool
    batch_button_apply = TkWidget(name="Apply changes")
    batch_button_apply.new_button(UI['text']['tools']['buttons']['apply'], options={
        'parent': batch_buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'randomizer', 'type': 'apply'}})
    
    #endregion

    # ###################################################################################################
    

    # OR: declare it before 'create buttons frame' and add 'buttons' key after based on which tool is selected to adapt the content?
    widgets_group_2 = { 'input': batch_input,
                        'preview': batch_result_preview,
                        'buttons': {}
                        }
    

    # Add graphical elements to the grid and pack all widgets into sidebar, to place and display them all together in the interface
    batch_tools.content.grid(row=0, pady=5)
    batch_button_clear.content.grid(row=0, sticky="e")
    batch_input.frame.grid()
    batch_buttons.frame.grid(pady=5, ipadx=10)                                                  # user_buttons.frame.config(labelanchor='n')      # configure(padx=50, pady=50) ? config(width=102) ?
    batch_button_add.content.pack(side=tk.LEFT, padx=2)                                         # rename_button.content.config(width=32, font=("TkDefaultFont", 10, "bold"), bg=_B1B, fg=_B1T, activebackground="lightblue", activeforeground="navy")
    batch_button_remove.content.pack(side=tk.LEFT, padx=2)                                      # edit_conditions_button.content.config(width=32, font=("TkDefaultFont", 10, "bold"), bg=_B2B, fg=_B2T, activebackground="lightpink", activeforeground="darkred")
    batch_button_move_up.content.pack(side=tk.RIGHT, padx=2)
    batch_button_move_down.content.pack(side=tk.RIGHT, padx=2)
    batch_button_preview.content.pack(side=tk.LEFT, padx=(80,0))
    batch_button_apply.content.pack(side=tk.RIGHT, padx=(0,80))
    batch_result_preview.frame.grid()
    batch_result_preview.frame.columnconfigure(0, weight=1)

    tk_sidebar.add(batch_tools.frame)
    tk_sidebar.add(preview.frame)

    # CONDITIONNER CE QU'ON AFFICHE COMME OUTIL DANS LA SIDEBAR (DIFFERENTS OUTILS SWITCHABLES) :
    # # # # add_button.content.pack(side=tk.LEFT, padx=35, pady=10)
    # # # # remove_button.content.pack(side=tk.RIGHT, padx=35, pady=10)
    # AJOUTER UNE OPTION DANS La BARRE DE MENU POUR PREVIEW/APPLY EN REALISANT LES CALCULS EN PRENANT EN COMPTE L'ANIMATION "DE BASE" (NON DAR)

    # Add the sidebar to the app's main window
    window.add(tk_sidebar)


def create_excluder(options={}):

    global UI, widgets_excluder

    def button_handler(input: str, action: str) -> None:        # MISSING DOCSTRING AND COMMENTS -- save_changes()
        if input == "excluder":
            if action == "remove": transfer_item(input=widgets_excluder['excluded_mods'], output=widgets_excluder['active_mods'])
            elif action == "add": transfer_item(input=widgets_excluder['active_mods'], output=widgets_excluder['excluded_mods'])
            elif action == "apply": apply_exclusions(input=widgets_excluder['excluded_mods'], callback=update_ui)
            elif action == "change_path": change_exclusions_auto_src_path()
            elif action == "auto": auto_update_exclusions_from_src(output={'active': widgets_excluder['active_mods'], 'excluded': widgets_excluder['excluded_mods']})

        else: return            # TO COMPLETE WITH OTHER TOOLS
    

    # Get data about mods being currently active and excluded
    excluder_data_mods = update_excluder_widgets()                                              # OR: pass UI values to the excluder as options?
    active_mods = excluder_data_mods.get('active')
    excluded_mods = excluder_data_mods.get('excluded')


    # Create main window (with its menu bar) and its rows and grid to hold the interface sections
    UI['window'] = tk.Tk()
    UI['window'].title("DART - DAR manager for TES V : Skyrim")
    # UI['window'].protocol("WM_DELETE_WINDOW", exit)
    tk_window = tk.PanedWindow(UI['window'], orient=tk.HORIZONTAL, bg=_BG)
    tk_window.pack(fill=tk.BOTH, expand=True)

    # Create a clickable list widget containing all active mods through detected user DAR mods
    active_mods_list = TkWidget(name=UI['text']['excluder']['active_mods'])
    active_mods_list.create_frame(tk_window, "list", active_mods, options={'f.bg': _BG})
    active_mods_list.content.config(width=70, height=50, bg=_WB)


    # Create buttons frame to pack the main set of excluder's buttons and divides it into 3 rows to place each button
    buttons = TkWidget(name="Buttons")
    buttons.create_frame(tk_window, "", "", options={'no_label': True, 'f.bg': _BG})            # buttons.frame.config(width=200, height=250)
    for i in range(3): buttons.frame.grid_rowconfigure(i, weight=1)

    # Create two buttons to add and remove a selected item (eg a detected user DAR mod) to the active mods list
    remove_exclusion_button = TkWidget(name="Remove item")
    remove_exclusion_button.new_button(UI['text']['excluder']['buttons']['remove'], options={
        'parent': buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'excluder', 'type': 'remove'}})
    remove_exclusion_button.content.grid(row=0, column=0, padx=10)
    
    add_exclusion_button = TkWidget(name="Add item")
    add_exclusion_button.new_button(UI['text']['excluder']['buttons']['add'], options={
        'parent': buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'excluder', 'type': 'add'}})
    add_exclusion_button.content.grid(row=0, column=2, padx=10)

    # Create a button to apply the new exclusions
    apply_exclusions_button = TkWidget(name="Apply changes")
    apply_exclusions_button.new_button(UI['text']['excluder']['buttons']['apply'], options={
        'parent': buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'excluder', 'type': 'apply'}})
    apply_exclusions_button.content.grid(row=1, column=1)

    # Create a button to change the auto-exclusion source file path (modlist file for MO2 users)
    path_exclusions_button = TkWidget(name="Change exclusion file path")
    path_exclusions_button.new_button(UI['text']['excluder']['buttons']['change_path'], options={
        'parent': buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'excluder', 'type': 'change_path'}})
    path_exclusions_button.content.grid(row=2, column=0)
    
    # Create a button to set automatically user's exclusions based on a chosen source file
    auto_exclusions_button = TkWidget(name="Set exclusions automatically")
    auto_exclusions_button.new_button(UI['text']['excluder']['buttons']['auto'], options={
        'parent': buttons.frame, 'w.bg': _WB, 'w.fg': _WT,
        'command': {'function': button_handler, 'input': 'excluder', 'type': 'auto'}})
    auto_exclusions_button.content.grid(row=2, column=2)


    # Create a clickable list widget containing all excluded mods through detected user DAR mods
    excluded_mods_list = TkWidget(name=UI['text']['excluder']['excluded_mods'])
    excluded_mods_list.create_frame(tk_window, "list", excluded_mods, options={'f.bg': _BG})
    excluded_mods_list.content.config(width=70, height=50, bg=_WB)

    # # Add graphical elements to the grid and pack all widgets into window, to place and display them all together in the interface
    active_mods_list.frame.pack()
    buttons.frame.pack()
    excluded_mods_list.frame.pack()

    tk_window.add(active_mods_list.frame)
    tk_window.add(buttons.frame)
    tk_window.add(excluded_mods_list.frame)


    widgets_excluder = {
        'active_mods': active_mods_list,
        'excluded_mods': excluded_mods_list,
        'buttons': {}
        }

    # Start the main loop
    UI['window'].mainloop()





# # # ################################################################################################### # # #
# # # ############################################## UNUSED ############################################# # # #
# # # ################################################################################################### # # #


# def create_loader():
#     loader = tk.Tk()
#     loader.title("DART - DAR manager for TES V : Skyrim")
#     tk_loader = tk.PanedWindow(loader, orient=tk.VERTICAL)
#     tk_loader.pack(fill=tk.BOTH, expand=True)

#     loading_header = TkWidget(name="Loading header")
#     loading_header.create_frame(tk_loader, "label",  "HEADER", options={'is_header': True, 'no_label': True})

#     loading_bar = TkWidget(name="Loading bar")
#     loading_bar.create_frame(tk_loader, "canvas", "", options={'no_label': True, 'no_scrollbar': True})
#     loading_bar.content.config(width=500, height=25, bg="black")

#     loading_log = TkWidget(name="Loading logs")
#     loading_log.create_frame(tk_loader, "canvas", "", options={'no_label': True, 'no_scrollbar': True})
#     loading_log.content.config(width=500, height=50, bg="green")

#     loading_header.content.grid(row=0, pady=(10,0))
#     loading_bar.content.pack(fill=tk.BOTH, expand=True)
#     loading_log.content.pack(fill=tk.BOTH, expand=True, pady=(5,30))

#     tk_loader.add(loading_header.frame)
#     tk_loader.add(loading_bar.frame)
#     tk_loader.add(loading_log.frame)

#     loader.mainloop()

#     # create_interface()


# TO COMMENT AND FINISH ########################
    # FILE_MENU.add_command(label="", state="disabled")                                 # add a disabled/fake item
    # FILE_MENU.delete(0)                                                               # remove the first line from the menu
    # FILE_MENU.add_command(label="Open", command=open_file)                            # Template to add menu item
    # FILE_MENU.add_command(label="Save", command=save_file)                            # Template to add menu item
    # # # Change root
    # Traitement du fichier sélectionné et prévisualisation de l'animation ici...
    # file_path = filedialog.askopenfilename(initialdir=str(MO2_ROOT), filetypes=[("HKX files", "*.hkx")])
    # dir_path = filedialog.askdirectory(initialdir=str(MO2_ROOT))