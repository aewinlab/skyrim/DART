from pathlib import Path


# MO2_ROOT = Path("D:\Mods\MO2-SSE\mods")                 # TEMPORARILY DISABLED
# MO2_ROOT = Path(__file__).parent / "mo2"                 # TEMPORARY MO2_ROOT PATH
# MO2_ROOT = Path(__file__).parent.parent / "[2] TESTS" / "mo2"                 # TEMPORARY MO2_ROOT PATH
MO2_ROOT = Path(__file__).parent.parent                 # GET MO2_ROOT PATH WHEN INSTALLED CORRECTLY

# SETTINGS_PATH = Path(__file__).parent / "user_settings.json"
SETTINGS_PATH = Path(__file__).parent / "gui" / "settings.json"
LANGUAGES_DIR = Path(__file__).parent / "gui" / "i18n"
THEMES_DIR = Path(__file__).parent / "gui" / "themes"

EXCLUSIONS_PATH = Path(__file__).parent / "settings" / "exclusions.json"
# MODLIST_PATH = Path(__file__).parent / "settings" / "mods.json"
