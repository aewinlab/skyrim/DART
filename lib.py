from pathlib import Path
import tkinter as tk
from dataclasses import dataclass, field
import logging


@dataclass
class Condition:
    priority: int
    path: Path
    conditions: list[str]
    animations: list[str]


@dataclass
class Mod:
    name: str
    path: Path
    conditions: list[Condition]


@dataclass
class TkWidget:
    name: str = ""
    frame: "tk.LabelFrame | tk.Frame" = tk.LabelFrame
    content: "tk.Listbox | tk.Canvas | tk.Label | tk.OptionMenu | tk.Entry | tk.Text | tk.Button" = tk.Canvas      # Added "" because of '|' not being accepted for exe creation (py-to-exe is py3.9 and introduction of '|' for types in python3.10+)
    options: dict[tk.StringVar] = field(default_factory=dict)

    def create_frame(self, parent, content_type, content, options={}):                                  # Create a new frame and its content
        if options.get('no_label'): self.frame = tk.Frame(parent, padx=15, pady=10)
        else: self.frame = tk.LabelFrame(parent, text=self.name, padx=15, pady=10)

        if options.get('f.bg'): self.frame.config(bg=options['f.bg'])
        if options.get('f.fg'): self.frame.config(fg=options['f.fg'])

        if content_type == "list": self.new_list(content, options)
        if content_type == "canvas": self.new_canvas(content, options)
        if content_type == "label": self.new_label(content, options)
        if content_type == "menu": self.new_menu(content, options)
        if content_type == "entry": self.new_entry(content)
        if content_type == "text": self.new_text(content)
        if content_type == "button": self.new_button(content, options)
        # if content_type == "label": self.frame = tk.Label(parent, text=content, padx=15, pady=15)
        # if content_type == "menu_bar": self.new_menu_bar(content, options)

    def new_list(self, content, options={}):                                                            # Create (or update) a listbox with its content into widget's frame
        # Create the list and set its content
        self.content = tk.Listbox(self.frame)
        for element in content:
            self.content.insert(content.index(element), f"{element}")
            if options.get('w.fg'): self.content.itemconfigure(content.index(element), fg=options['w.fg'])

        if options.get('w.bg'): self.content.config(bg=options['w.bg'])

        # Add a scrollbar to the list
        self.new_scrollbar()
    
    def new_canvas(self, content, options={}):                                                          # Create (or update) a canvas with its content into widget's frame
        # Create the canvas and set its content
        self.content = tk.Canvas(self.frame)
        # self.content = tk.Canvas(self.frame, background="white")
        if options.get('w.bg'): self.content.config(bg=options['w.bg'])
        text = "\n".join([str(element) for element in content])
        self.content.create_text(10, 10, text=text, tags=self.name, font="Arial 10 italic", fill="blue", anchor="nw")
        # canvas = self.content.create_text(10, 10, text=text, tags=self.name, font="Arial 10 italic", fill="blue", anchor="nw")

        # Add a scrollbar to the canvas
        if not options.get('no_scrollbar'): self.new_scrollbar(options={'is_canvas': True})
        # if not options.get('no_scrollbar'):                                                               # ALT VERSION - TO REMOVE?
        #     options['is_canvas'] = True
        #     self.new_scrollbar(options)

    def new_scrollbar(self, options={}):                                                                # Create a scrollbar into widget's frame
        # parent = options['parent'] if options.get('parent') else self.frame       # # ALT VERSION - TO REMOVE? NB: AND REPLACE self.frame BY parent BELOW

        # Place and set dimension of the widget's frame and content
        self.content.grid(row=0, column=0, sticky="nswe")
        self.frame.columnconfigure(0, weight=1)

        # Add a scrollbar to the frame
        scrollbar = tk.Scrollbar(self.frame, orient=tk.VERTICAL)
        scrollbar.config(command=self.content.yview)
        scrollbar.grid(row=0, column=1, sticky="ns")
        self.content.config(yscrollcommand=scrollbar.set)

        # Set scrolling limits if parent widget is a canvas
        if options.get('is_canvas'): self.content.config(scrollregion=(0, 0, 0, 0))

    def new_menu(self, content: dict, options={}):                                                      # Create (or update) a dropdown menu with its content into widget's frame
        # Create a list of options for the dropdown menu from a dict keys
        self.options['stringvar'] = tk.StringVar(self.frame)
        choices = [key for key in content]

        # Set menu's default value
        self.options['stringvar'].set(choices[0])

        # Create the dropdown menu itself and set its content
        self.content = tk.OptionMenu(self.frame, self.options['stringvar'], *choices)
        self.content.pack()

        if options.get('w.bg'): self.content.config(bg=options['w.bg'])
        if options.get('w.bg:a'): self.content.config(activebackground=options['w.bg:a'])
        if options.get('w.fg'): self.content.config(fg=options['w.fg'])
        if options.get('w.fg:a'): self.content.config(activeforeground=options['w.fg:a'])
        if options.get('w.hb'): self.content.config(highlightbackground=options['w.hb'])
    
    def new_entry(self, content=None):                                                      # Create (or update) an entry input with its content into widget's frame
        # Create the entry line input
        self.content = tk.Entry(self.frame)
        self.content.pack()

        # Set its content if needed (useless if content == None)
        if content:
            self.content.insert(tk.END, content)

    def new_text(self, content=None):                                                       # Create (or update) a text input with its content into widget's frame
        # Create the text area input
        self.content = tk.Text(self.frame)

        # Set its content if needed (useless if content == None)
        if content:
            self.content.insert(tk.END, content)
            self.content.pack()

        # Add a scrollbar to the text input
        self.new_scrollbar()

    def new_button(self, content, options={}):                                              # Create (or update) a button with its content and a binded function into widget's frame or a parent widget
        # Create the button (into a parent widget if given into options parameter) and set its content
        if options.get('parent'): self.content = tk.Button(options['parent'], text=content)
        else: self.content = tk.Button(self.frame, text=content)

        if options.get('w.bg'): self.content.config(bg=options['w.bg'])
        if options.get('w.fg'): self.content.config(fg=options['w.fg'])

        # Set its binded command if given into options parameter
        if options.get('command'):
            on_click = options['command']['function']
            input = options['command']['input']
            action = options['command']['type']

            if options['command'].get('output'):
                output = options['command']['output']
                self.content.config(command=lambda: on_click(input, output, action))                        # consider adding vars or options for optional parameters (and set to {} by default)

            else: self.content.config(command=lambda: on_click(input, action))
    
    def new_label(self, content, options={}):
        # self.content = tk.Label(self.frame, text=content, font=("Helvetica", 12), bg="red")
        self.content = tk.Label(self.frame, text=content)

        if options.get('f.bg'): self.content.config(bg=options['f.bg'])
        if options.get('f.fg'): self.content.config(fg=options['f.fg'])

        if options.get('is_header'): self.content.config(font=("TkDefaultFont", 10, "bold"))
    
    # def new_menu_bar(self, content, options={}):            # MISSING COMMENTARIES
    #     pass


# USELESS ???
@dataclass
class AppComponents:
    main: "tk.Tk() | None"
    popup: "tk.Tk() | None"

    def kill(self, component: str):
        if component == "main":
            self.main.destroy()
            self.main = None
        if component == "popup":
            self.popup.destroy()
            self.popup = None
    
    def hide(self, component: str):
        if component == "main":
            self.main.withdraw()
            # if self.popup: self.popup.protocol("WM_DELETE_WINDOW", self.main.destroy())
        if component == "popup": self.popup.withdraw()
    
    def unhide(self, component: str):
        if component == "main": self.main.deiconify()
        if component == "popup": self.popup.deiconify()


# TO REMOVE? BUGGED OUT ALL INTERFACE, MAYBE ALREADY EXISTING IN TKINTER?
# @dataclass
# class TkInterface:
#     window: tk.Tk
#     text: dict
#     theme: dict

#     def create_interface(self, window=tk.Tk(), text={}, theme={}):
#         self.window = window
#         self.text = text
#         self.theme = theme