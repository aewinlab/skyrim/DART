from pathlib import Path
import logging

from constants import MO2_ROOT, EXCLUSIONS_PATH
from lib import Condition, Mod
from utils.file_io import get_lines, strip_words, load_json



def get_dar_mods(folder: Path) -> list[Path]:                                                         # RENAME TO detect_user_dar_mods() path ?

    return folder.rglob("_CustomConditions")                              # Alternates: MO2_ROOT.rglob("DynamicAnimationReplacer") ; MO2_ROOT.rglob("_conditions.txt") ; +/- if != "DAR - DynamicAnimationReplacer SE v1.1.0"



def get_conditions(folder: Path, exclude={}) -> list[Mod]:                                                       # RENAME TO # get_user_data() mods & conditions ?

    output = []
    excluded_mods = exclude.get('mods')

    for mod in get_dar_mods(folder):

        mod_name = mod.relative_to(folder).parts[0]
        if mod_name in excluded_mods: continue         # check if mod_name is not in exclusion list

        mod_conditions = []
        conditions = [element for element in mod.glob("*") if element.name.isdigit() and element.is_dir()]

        for condition in conditions:        # récupère tous les dossiers dans le répertoire _CustomConditions du mod

            if condition / "_conditions.txt" in condition.glob("*"):        # vérifie la validité de la structure du dossier
                conditions_file = condition / "_conditions.txt"
                conditions_by_line = []

                for line in get_lines(conditions_file):
                    stripped_line = strip_words(line, ("AND", "OR", " "))
                    if stripped_line not in conditions_by_line: conditions_by_line.append(stripped_line)

                animations = [animation.name for animation in condition.rglob("*.hkx")]

                new_condition = Condition(priority=int(condition.name), path=condition, conditions=conditions_by_line, animations=animations)
                mod_conditions.append(new_condition)

            else: print(f"{mod_name}::{condition}: no '_conditions.txt' file found")

        new_mod = Mod(name=mod_name, path=mod, conditions=mod_conditions)
        output.append(new_mod)
    
    return output



def fetch_gui_data(data: list[Mod], exclude={}) -> dict:

    all_mods_detected = []
    all_conditions_detected = []
    all_animations_detected = []
    all_priorities_detected = {}

    excluded_conditions = exclude.get('conditions')                 # detect all excluded conditions from user's exclusion lists

    for mod in data:

        if not mod.name.lower() in [e.lower() for e in all_mods_detected]:
            all_mods_detected.append(mod.name)                                              # Get all DAR mods detected

        for condition in mod.conditions:
            update_with_new_elements(condition.conditions, all_conditions_detected, excluded_conditions)         # Get all DAR functions detected in _conditions.txt
            update_with_new_elements(condition.animations, all_animations_detected)         # Get all animations detected in DAR folders

            update_with_duplicates(condition.priority, all_priorities_detected, mod.name)   # Get all duplicated DAR priorities detected in DAR folders
    
    # Get all duplicated priority values and corresponding mods from user's data
    duplicated_priorities = { priority: all_priorities_detected[priority] for priority in all_priorities_detected if len(all_priorities_detected[priority]) > 1 }

    # Format all_priorities_detected into a list
    all_priorities_detected = [priority for priority in all_priorities_detected]

    # Sort all detected mods, animations, conditions alphabetically, and all priorities numerically
    all_mods_detected.sort(key=str.lower)
    all_animations_detected.sort(key=str.lower)
    all_conditions_detected.sort(key=str.lower)
    all_priorities_detected.sort(key=int)
    detected_elements = { "Animations": all_animations_detected, "Conditions": all_conditions_detected, "Mods": all_mods_detected, "Priorities": all_priorities_detected }

    return { "categories": detected_elements, "duplicates": duplicated_priorities }



def update_with_new_elements(chunk: list[str], data: list[str], excluded_elements: list[str] = [], alt_updated_value=None) -> None:

    for element in chunk:
        if element.lower() in [e.lower() for e in data]: continue
        if element.lower() in [e.lower() for e in excluded_elements]: continue                      # check if element is not in exclusion list
        data.append(element) if alt_updated_value == None else data.append(alt_updated_value)



def update_with_duplicates(element: str, data: dict, alt_updated_value=None) -> None:

    if not element in data: data[element] = [alt_updated_value] if alt_updated_value != None else [element]
    else: data[element].append(alt_updated_value) if alt_updated_value != None else data[element].append(element)
