from pathlib import Path
import logging
import json


def save_data(data: dict, dir_path: Path):

    logging.debug(f"File format is {dir_path.suffix}")

    if dir_path.suffix == ".json":
        
        with dir_path.open('w', encoding='utf-8') as file:
            json.dump(data, file, ensure_ascii=False, indent=4)
        
        logging.info(f"Saved data into {dir_path}")



def load_json(file_path: Path) -> dict:

    logging.debug(f"File format is {file_path.suffix}")

    if file_path.suffix == ".json":
    
        with open(file_path, 'r', encoding='utf-8') as file:
            return json.load(file)



def save_text(text: str, file_path: Path):
    
    with open(file_path, 'w') as file:
        file.write(text)



def get_lines(f: Path) -> list[str]:
    if f.suffix != ".txt": return []

    with open(f, 'r', encoding='utf-8') as text:
        lines = text.read().splitlines()

    return lines



def strip_words(s: str, words: tuple[str, ...]):

    while s.endswith(words) or s.startswith(words):
        for word in words:
            if s.startswith(word): s = s.lstrip(word)
            elif s.endswith(word): s = s.rstrip(word)

    return s



def clean_lines(lines: list[str]) -> list[str]:
    cleaned_lines = []
    
    for line in lines:
        stripped_line = line.strip(" ")
        if stripped_line in ("\n", ""): continue
        cleaned_lines.append(stripped_line)

    return cleaned_lines
