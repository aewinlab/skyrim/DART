# darfunctions

from lib import Condition, Mod



# P(i) = 1/(n-i) with n = len(priorities) (number of priorities), i = index(itération) (current priority)       (previously = random_proba_calc() ou random_uniform_proba())
# Examples: 1 = 1    ||    2 = 0.5 - 1 ; 3 = 0.33 - 0.5 - 1    ||    4 = 0.25 - 0.33 - 0.5 - 1    ||    5 = 0.2 - 0.25 - 0.33 - 0.5 - 1
def uniform_randomizer(priorities: list[int], decimals: int = 2, options={}) -> list[dict]:

    # if options.get('is_using_base'): priorities.append(-9999999999)         # Or use -9999999999 (instead of append("base"))
    priorities.sort(key=int)
    priorities.reverse()
    if options.get('is_using_base'): priorities.append("base")                  # If the base animation in the SSE root is being utilized in the computation

    n = len(priorities)
    return [{ "priority": i, "proba": round((1 / (n - priorities.index(i))), decimals) } for i in priorities]
    # return [{ i: round((1 / (n - priorities.index(i))), decimals) } for i in priorities]
    # OR return [{ "condition": i, "probability": round((1 / (n - priorities.index(i))), decimals) } for i in priorities]

# Ajouter une option pour intégrer comme base l'animation de base (non DAR) --> DONE
# L'intégrer dans interface avec une option dans le menu settings ? (et le passer en argument le cas échéant si activé)




USER_DATA = []      # get_user_data()
# sélectionner 2 mods, en prioriser un et reclasser en fonction de l'ensemble des priorités présentes
def prioritize_a_mod_over_another(prioritized_mod: Mod, overtaken_mod: Mod):
    max_priority_to_overtake = max([condition.priority for condition in overtaken_mod.conditions])
    number_to_prioritize = len([condition.priority for condition in prioritized_mod.conditions])

    priorities = []
    for mod in USER_DATA:
        for condition in mod.conditions:
            if condition.priority not in priorities: priorities.append(condition.priority)
    # test = [condition.priority for condition in [mod.conditions for mod in USER_DATA]]
    # test = [condition.priority for condition in [mod.conditions for mod in USER_DATA]]
    # test = [mod.conditions for mod in USER_DATA]
    priorities.sort()

    [1 + max_priority_to_overtake + n for n in range(number_to_prioritize)]

    for priority in priorities:
        if priority >= max_priority_to_overtake:
            i = priorities.index(priority)
            if i == len(priorities) - 1: return priorities[-1] + 1
            if priorities[i + 1] < priority + number_to_prioritize: continue

    print(priorities)

# print([1 + 3000 + n for n in range(4)])